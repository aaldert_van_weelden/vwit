<?php 
/**
 * Update a git based dependency by updating the commit reference in the composer lock file
 * 
 * Use this to synchronise your own project development dependencies hosted on a git-repo 
 *  
 * @author Aaldert van Weelden
 * 
 */



class UpdateComposerLock{
	
		
	const SOURCE_ROOT = 0;
	const DESTINATION_ROOT = -3;
	
	private $current;
	private $target;
	private $lockfile;
	private $lockfileContent;
	
	public function __construct($current, $target, $lockfile){
			
		$this->current = $current;
		$this->target = $target;
		$this->lockfile = $lockfile;
	}
		
	public function run(){
		$content = file_get_contents($this->lockfile);
		
		if($content !== null && !empty($content) && is_string($content)){
			$this->lockfileContent = $content;
		}else{
			die('ERROR: provided file is null or empty');
		}
		
	}
	
	
	public function generate(){
		print 'updating lockfile...'."\n";
		
		$file = fopen($this->lockfile, "w");
		
		
		
		$content = $this->lockfileContent;
		
			
		$this->lockfileContent = str_replace($this->current,$this->target,$content);
		
		fputs($file, $this->lockfileContent);
		fclose($file);
	}

	

}

print "\nupdating composer.lock...\n";


$argc = $GLOBALS['argc']; //number of arguments passed
$argv = $GLOBALS['argv']; //the arguments passed
		
if(count($argv)<2){
	die('WARNING please provide the current commit hash'."\n\n");
}

if(count($argv)<3){
	die('WARNING please provide the commit hash to update to'."\n\n");
}
		
		
$current = $argv[1];
$target = $argv[2];
$lockfile = 'composer.lock';



$generator = new UpdateComposerLock($current, $target, $lockfile);
$generator->run();

print "\n ---READY---";

?>