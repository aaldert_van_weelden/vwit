CKEDITOR.plugins.add( 'videoles',
{   
   requires : ['richcombo'], //, 'styles' ],
   init : function( editor )
   {
      var config = editor.config,
         lang = editor.lang.format;

     
       /* 
		var VIDEOLESSONS = [
			{"key" : "1","i18n_name" : "Start de videoles","value" : "Ocean impression","description" : "VGhlIHd","assets" : "ZGVtbyB2aWRlbyBzdH","owner" : "Globaal"},
			{"key" : "2","i18n_name" : "Start de videoles","value" : "Sint lauda...","description" : "SGljIGV1bS4=","assets" : "TmloaWwgbmV","owner" : "Cadenza"},
		];

       */
      
      
      // Create style objects for all defined styles.

      editor.ui.addRichCombo( 'tokens',
         {
            label : "Videoles",
            title :"Videoles",
            voiceLabel : "Videoles",
            className : 'cke_format',
            multiSelect : false,

            panel :
            {
               css : [ config.contentsCss, CKEDITOR.getUrl(CKEDITOR.skinName.split(",")[1]||"skins/"+CKEDITOR.skinName.split(",")[0]+"/") + "editor.css" ],
               voiceLabel : lang.panelVoiceLabel
            },

            init : function()
            {
               this.startGroup( "Videoles" );
               var that = this;
               var button;
               //this.add('value', 'drop_text', 'drop_label');
               $.each(VIDEOLESSONS, function(index, lesson){
            	   button = '<button class="btn btn-large btn-primary btn-videolesson onclick" title="'+lesson.value+'" name="btn-videolesson" id="'+lesson.key+'">'+lesson.i18n_name+'</button>';
                   that.add(button, lesson.value, $.base64decode(lesson.description));
               });
               	  
               
            },

            onClick : function( value )
            {         
               editor.focus();
               editor.fire( 'saveSnapshot' );
               //editor.insertHtml(value);
               editor.insertHtml(value);
               editor.fire( 'saveSnapshot' );
            }
         });
   }
});