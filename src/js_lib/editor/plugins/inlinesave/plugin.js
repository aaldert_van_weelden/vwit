CKEDITOR.plugins.add( 'inlinesave',
{
	init: function( editor )
	{
		editor.addCommand( 'inlinesave',
			{
				exec : function( editor )
				{

					addData();
					
					function addData() {
						var data = editor.getData();
						var containerID = editor.container.getId();
						var page = editor.container.getAttribute('page');
						var paragraph = editor.container.getAttribute('paragraph');
						
						if(typeof(page)==='undefined' || page==null || page==''){
							page=null;
						}
						
						if(typeof(paragraph)==='undefined' || paragraph==null || paragraph==''){
							paragraph=null;
						}
						
						PagePersistForm.savePageElement(page,paragraph,containerID,data);

					} 

				}
			});
		editor.ui.addButton( 'Inlinesave',
		{
			label: 'Save',
			command: 'inlinesave',
			icon: this.path + 'images/inlinesave.png'
		} );
	}
} );