﻿/*
 Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
*/

/*
var INPAGE_QUESTIONS = [{
    title: "Vraag 1",
    image: "question-mark.png",
    description: "Een vraag over de zin van het leven",
    html: '<h3><img src=" " alt="" style="margin-right: 10px" height="100" width="100" align="left" />Titel hier</h3><p>Tekst hier</p>'
}, {
    title: "Vraag 2",
    image: "question-mark.png",
    description: "Een vraag naar de bekende weg",
    html: '<table cellspacing="0" cellpadding="0" style="width:100%" border="0"><tr><td style="width:50%"><h3>Titel 1</h3></td><td></td><td style="width:50%"><h3>Titel 2</h3></td></tr><tr><td>Tekstt 1</td><td></td><td>Tekstt 2</td></tr></table><p>Meer tekst.</p>'
}, {
    title: "Vraag 3",
    image: "question-mark.png",
    description: "Voor u een vraag , voor ons een weet.",
    html: '<div style="width: 80%"><h3>Titel hier</h3><table style="width:150px;float: right" cellspacing="0" cellpadding="0" border="1"><caption style="border:solid 1px black"><strong>Tabel titel</strong></caption><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table><p>Tekst hier</p></div>'
}];
*/

CKEDITOR.addTemplates("default", {
    imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"),
    templates: INPAGE_QUESTIONS
    
});