CKEDITOR.plugins.add( 'cursus',
{   
   requires : ['richcombo'], //, 'styles' ],
   init : function( editor )
   {
      var config = editor.config,
         lang = editor.lang.format;

     
       /* 
		var VIDEOLESSONS = [
		var COURSES = [
	{"key" : "30","i18n_name" : "De leerkracht maakt efficiënt gebruik van de geplande onderwijstijd","value" : "DE LEERKRACHT MAAKT EFFICIëNT GEBRUIK VAN DE GEPLANDE ONDERWIJSTIJD","owner" : 1},
	{"key" : "32","i18n_name" : "De leerkracht bevordert het zelfvertrouwen van de leerlingen","value" : "DE LEERKRACHT BEVORDERT HET ZELFVERTROUWEN VAN DE LEERLINGEN","owner" : 1},
			];

       */
      
      
      // Create style objects for all defined styles.
      
   

      editor.ui.addRichCombo( 'tokens',
         {
            label : "Cursus",
            title :"Cursus",
            voiceLabel : "Cursus",
            className : 'cke_format',
            multiSelect : false,

            panel :
            {
               css : [ config.contentsCss, CKEDITOR.getUrl(CKEDITOR.skinName.split(",")[1]||"skins/"+CKEDITOR.skinName.split(",")[0]+"/") + "editor.css" ],
               voiceLabel : lang.panelVoiceLabel
            },

            init : function()
            {
               this.startGroup( "Cursus" );
               var that = this;
               var button;
               //this.add('value', 'drop_text', 'drop_label');
               $.each(COURSES, function(index, course){
            	   button = '<button class="cke-btn-course onclick" title="'+course.i18n_name+'" name="btn-course" id="'+course.key+'">'+course.i18n_name+'</button>';
                   that.add(button, course.i18n_name, course.i18n_name);
               });
  
            },
            
           

            onClick : function( value )
            {         
               editor.focus();
               editor.fire( 'saveSnapshot' );
               //editor.insertHtml(value);
               editor.insertHtml(value);
               editor.fire( 'saveSnapshot' );
            }
         });
   }
});