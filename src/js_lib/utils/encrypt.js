/*
* RSA, a class to rsa-encrypt a string in
* JavaScript.

* encrypter written by Daniel Griesser
* dave@ohdave.com
* http://www.jcryption.org/
* Daniel Griesser <daniel.griesser@jcryption.org>
*
* changed and improved to OOP/namespacing by Aaldert van Weelden 2010
* http://www.aaldertvanweelden.nl/
* Aaldert van Weelden info@aaldertvanweelden.nl>
*/

/*
add to css stylesheet :
.disabled {
    background             : none;
    background-color       : #e0e0e0;
    color                  : #fff;     
}

*/
var RSA = (function(){

        //private constants
        var MSG_ERROR = 'Er is een serverfout opgetreden, probeer het nog eens',
            CNT_MESSAGE = '#message',
            //predefined options
            oOptions = {
                form            : "",
                fieldsToEncrypt : [],
                type            : "xhr-post",//xhr-post, http-get, http-post
                beforeSubmit    : function(){},
                submitSuccess : function(oResponse){
                    var $cntMessage=$(CNT_MESSAGE);
                    $cntMessage.html(oResponse.data);
                    $cntMessage.show(500);
                    return true;
                },
                submitFailure   : function(oResponse){
                    var $cntMessage=$(CNT_MESSAGE);
                    $cntMessage.html(MSG_ERROR+oResponse.status);
                    $cntMessage.show(500);
                    return false;
                },
                disableBtn      : false,
                posturl         : 'xhr/submit.php',
                geturl          : 'xhr/submit.php',
                decrypturl      : 'xhr/decrypt.php',
                keyurl          : 'xhr/key.php',
                btnEncrypt      : '#encrypt',
                encodeURI       : 'base64',//base64, base64safe, false or '' = nodecode
                hash            : true,
                noSubmit        : {}//fieldnames not to be submitted
            },
        
            //private vars
            form,
            $form,
            oSubmit,
            bRequestActive=false,
            oKeys,
            oCallback,
            oBtn,
            doc,
            oStringPair = {
                ToEncrypt   : '',
                IsEncrypted : ''
            };
            
        //private constructors
		var KeyPair = function(encryptionExponent, modulus, maxdigits) {
            bi.fn.setMaxDigits(maxdigits);
            this.e = bi.fn.FromHex(encryptionExponent);
            this.m = bi.fn.FromHex(modulus);
            this.chunkSize = 2 * bi.fn.HighIndex(this.m);
            this.radix = 16;
            this.barrett = new BarrettMu(this.m);
		};
		
		var _JWT = {

				jwt   : {},
				claim : {},

				/**
				 * Decodes the JWT
				 * @param jwt
				 * @returns {*}
				 */
				decodeToken : function(jwt) {
					var a = jwt.split(".");
					/*
					 * native b64utos failes unexpectedly, see
					 * https://github.com/kjur/jsjws/issues
					 */
					//return b64utos(a[1]);
					return $.base64decode(a[1]);
				},
				
				/**
				 * Parse the response JSON string and chache the JWT
				 */
				parseJwt : function(data){
					this.setJwt(JSON.parse(data).data);
				},

				/**
				 * Sets the JWT to the store object
				 * @param data
				 */
				setJwt : function(data) {
					this.jwt = data;
					this.claim = this.decodeToken(data);
				},

				getJwt : function() {
					return this.jwt;
				},

				/**
				 * Retrieve the JWT claim
				 * @returns {*}
				 */
				getClaim : function() {
					return JSON.parse(this.claim);
				}

		};

        //encrypter
        var _doEncrypt = function(sInput, callback){

            var charSum = 0;
            for(var i = 0; i < sInput.length; i++){
                charSum += sInput.charCodeAt(i);
            }
            var tag = '0123456789abcdef';
            var hex = '';
            hex += tag.charAt((charSum & 0xF0) >> 4) + tag.charAt(charSum & 0x0F);

            var taggedString = hex + sInput;
            var aEncrypt = [];
            var j = 0;

            while (j < taggedString.length) {
                aEncrypt[j] = taggedString.charCodeAt(j);
                j++;
            }

            while (aEncrypt.length % oKeys.chunkSize !== 0) {
                aEncrypt[j++] = 0;
            }
            
            function encryption(encryptObject) {
                var charCounter = 0;
                var j, block;
                var encrypted = "";
                function encryptChar() {
                    block = new BigInt();
                    j = 0;
                    for (var k = charCounter; k < charCounter+oKeys.chunkSize; ++j) {
                        block.digits[j] = encryptObject[k++];
                        block.digits[j] += encryptObject[k++] << 8;
                    }
                    var crypt = oKeys.barrett.powMod(block, oKeys.e);
                    var text = oKeys.radix == 16 ? bi.fn.ToHex(crypt) : bi.fn.ToString(crypt, oKeys.radix);
                    encrypted += text + " ";
                    charCounter += oKeys.chunkSize;
                    if (charCounter < encryptObject.length) {
                        setTimeout(encryptChar, 1)
                    } else {
                        oStringPair.IsEncrypted = encrypted.substring(0, encrypted.length - 1);
                        if($.isFunction(callback)) {
                            callback(oStringPair.IsEncrypted);
                        } else {
                            return oStringPair.IsEncrypted;
                        }
                    }
                }
                setTimeout(encryptChar, 1);
            }
            encryption(aEncrypt);
        };
        
        var _disableFields = function(name){
            if(typeof(name)==='undefined'){
                for(name in form.fields){
                    form.self[name] && form.self[name].setAttribute ? form.self[name].setAttribute('disabled','disabled'):void(0);
                }
            }
            else{
                name=name.replace('#','');
                if(form.self[name]){
                    form.self[name].setAttribute('disabled','disabled');
                }
                var btn=$('#'+name);
                btn.attr("disabled","disabled")
                btn.addClass('disabled')
            }
        };
        //gets server keys
        var _keys = { //_xhr-interface
			
            set : function(oResponse) {
                bRequestActive=false;
                var oData = $.JSONparse(oResponse.data);
                oKeys = new KeyPair(oData.e, oData.n, oData.maxdigits);
                if($.isFunction(oCallback)) {
                    oCallback(oKeys);
                } 
			},
			
			setJwt : function(oResponse) {
                bRequestActive=false;
                _JWT.parseJwt(oResponse.data);
                var keys = _JWT.getClaim().data;
                oKeys = new KeyPair(keys.e, keys.n, keys.maxdigits);
                if($.isFunction(oCallback)) {
                    oCallback(_JWT.getJwt());
                } 
			},
            
            doError : function(){
                bRequestActive=false;
                alert(MSG_ERROR);
                if($.isFunction(oCallback)) {
                    oCallback(false);
                } 
            }
	
		};
	
		var _xhr = {
			//req 01-----------------------------------------
			getServerKey : function(callback){
                oCallback=callback;//global storage
				$.submit({
					type 			: "get",
					url 			: oOptions.keyurl,
					onsuccess 		: this.getServerKeySuccessResult,
					onfailure 		: this.getServerKeyFailureResult,
					scope			: self //callbackscope
					
				});
			},
				
			getServerKeySuccessResult : function(oResponse) {
				_keys.set(oResponse,true);
			},
			
			getServerKeyFailureResult : function(oResponse) {
				_keys.doError(oResponse,false);
			},
			
			//req 01a-----------------------------------------
			getServerJwtKey : function(callback){
                oCallback=callback;//global storage
				$.submit({
					type 			: "get",
					url 			: oOptions.keyurl,
					onsuccess 		: this.getServerJwtKeySuccessResult,
					onfailure 		: this.getServerJwtKeyFailureResult,
					scope			: self //callbackscope
					
				});
			},
				
			getServerJwtKeySuccessResult : function(oResponse) {
				_keys.setJwt(oResponse,true);
			},
			
			getServerJwtKeyFailureResult : function(oResponse) {
				_keys.doError(oResponse,false);
			},
            
            //req 02-----------------------------------------
            submit : function(obj){
                $.submit({
                    type 			: obj.type,
                    encode          : oOptions.encodeURI,//base64, base64safe, utf8
                    json            : true,
                    url 			: obj.url,
                    data			: 'd='+obj.data+'&page='+obj.page,
                    onsuccess 		: this.submitSuccessResult,//200
                    onfailure 		: this.submitFailureResult,//404 ea
                    scope           : self       
                });
            },
                    
            submitSuccessResult : function(oResponse){
                oOptions.submitSuccess(oResponse);
                bRequestActive=false;
            },
                    
            submitFailureResult : function(oResponse){
                oOptions.submitFailure(oResponse);
                bRequestActive=false;
            }
        };
        
        var _serialize = {

            serializeForm : function(Form, Fields, bMode){
                
                var oForm,
                    mode = bMode ? 'true':'false',
                    serial,
                    obj = {
                        "inclusion"		: bMode
                    },
                    oFields;
                //"overloading"
                $.getExactType(Form)==='String' ? oForm=$.getRef(Form) : oForm=Form;
                $.getExactType(Fields)==='Array' ? oFields=$.arrayToObj(Fields,mode) : oFields=Fields;
                
                $.extend(obj, oFields);
                serial=$.getForm(oForm,obj,{
                    txtEncode : ['Email'],//field base64 encoded
                    noStore   : true
                });
                    
                return {
                    self                : oForm,
                    fields              : oFields,
                    uricomponent        : serial 
                };
            },
                
            andEncryptBeforeSubmit : function(options){
                this.setOptions(options);
                form =  this.serializeForm(oOptions.form, oOptions.fieldsToEncrypt, true);
                $form = $(form.self);
                $form.unbind('beforeSubmit').bind('beforeSubmit',function(event,obj){
                    oOptions.beforeSubmit.call($,obj);
                });
                $.encrypt(form.uricomponent, RSA.serialize.FormAndSubmit);
            },

            FormAndSubmit : function(sEncrypted){
                    
                var field = $('<input type="hidden" name="safe" value="'+sEncrypted+'" />'),
                    sSubmit,
                    oClientKeys;
                            
                $form.append(field);
                       
                //for regular post submission disable safe fields in the form and add action to form [no default action for safety]
               // _disableFields();
                
                $.extend(form.fields,oOptions.noSubmit);
                oSubmit = this.serializeForm(form.self, form.fields, false);
                oClientKeys=$.getClientKeys();//globals plus hash
                
                if(oOptions.hash){
                    sSubmit=$.safeURI(oSubmit.uricomponent+'&hash='+oClientKeys.hash, oOptions.encodeURI);
                }
                else{
                    sSubmit=$.safeURI(oSubmit.uricomponent, oOptions.encodeURI);
                }
                
                //---------------
                var sw=oOptions.type.split('-');
                        
                switch(sw[0]){
                    case 'xhr' :
                        var obj = {
                                type        : sw[1],
                                url         : sw[1]==='post' ? oOptions.posturl : oOptions.geturl,
                                data        : sSubmit,
                                encrypted   : sEncrypted,
                                form        : form.self 
                        };
                        $.extend(obj,oClientKeys);
                        $form.trigger('beforeSubmit',obj);
                        if(oOptions.disableBtn){
                            _disableFields(oOptions.btnEncrypt);//disable encrypt button
                        }
                        break;
                                
                    case 'http' :
                        if(sw[1]==='post'){
                            $form.attr('action',oOptions.decrypturl);
                            form.self.submit();
                        }
                        else{
                            $form.attr('action',oOptions.decrypturl+'?d='+sSubmit);
                            $form.attr('method','get');
                            form.self.submit();
                        }
                        break;             
                }
            },
                    
            andSubmit : function(obj){
                        _xhr.submit(obj);
                    },

            setOptions : function(options){
                $.extend(oOptions, options);
            }
        };

        var self = {
            
            rsaGetKey : function(callback){
                $(CNT_MESSAGE).hide()
                _xhr.getServerKey(callback);
            },
            
            rsaGetJwtKey : function(callback){
                $(CNT_MESSAGE).hide()
                _xhr.getServerJwtKey(callback);
            },
            
            encrypt : function(value, callback){
                return _doEncrypt(value, callback);
            },
            
            getJWT : function(){
            	return _JWT.getJwt();
            },
            
            setJWT : function(jwt){
            	return _JWT.setJwt(jwt);
            },
            
            getClaim : function(){
            	return _JWT.getClaim();
            },
            
            serialize : {
            
                andEncryptBeforeSubmit : function(options){
                    _serialize.andEncryptBeforeSubmit(options);
                },
                
                FormAndSubmit : function(string){
                    _serialize.FormAndSubmit(string);
                },
                
                andSubmit : function(obj){
                    _serialize.andSubmit(obj);
                },
                
                setOptions : function(options){
                    _serialize.setOptions(options);
                },
                
                disableFields : function(name){
                    _disableFields(name);
                }
                
            }
        };
        return self;
})();

/*
* BigInt, a suite of routines for performing multiple-precision arithmetic in
* JavaScript.
* BarrettMu, a class for performing Barrett modular reduction computations in
* JavaScript.
*
*
* Copyright 1998-2005 David Shapiro.
* dave@ohdave.com
*
* changed and improved by Daniel Griesser
* http://www.jcryption.org/
* Daniel Griesser <daniel.griesser@jcryption.org>
*
* changed and improved to OOP/namespacing by Aaldert van Weelden 2010
* http://www.aaldertvanweelden.nl/
* Aaldert van Weelden info@aaldertvanweelden.nl>
*/
var bi = {
    identifier : "bigint",
    ZERO_ARRAY : []
};

BigInt = function(flag) {
    if (typeof flag == "boolean" && flag == true) {
        this.digits = null;
    }
    else {
        this.digits = bi.ZERO_ARRAY.slice(0);
    }
    this.isNeg = false;
}

bi.fn = (function(){

    
    //PRIVATE VARS

    var bigRadixBits = 16,
        bitsPerDigit = bigRadixBits,
        bigRadix = 1 << 16,
        bigHalfRadix = bigRadix >>> 1,
        bigRadixSquared = bigRadix * bigRadix,
        maxVal = 1000000000000000,
        maxDigitVal = bigRadix - 1,
        maxInteger = 9999999999999998,
        dpl10 = 15,
        
        highBitMasks = new Array(0x0000, 0x8000, 0xC000, 0xE000, 0xF000, 0xF800,
            0xFC00, 0xFE00, 0xFF00, 0xFF80, 0xFFC0, 0xFFE0,
            0xFFF0, 0xFFF8, 0xFFFC, 0xFFFE, 0xFFFF
        ),
        hexatrigesimalToChar = new Array(
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z'
        ),
        hexToChar = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f'
        ),
        lowBitMasks = new Array(0x0000, 0x0001, 0x0003, 0x0007, 0x000F, 0x001F,
            0x003F, 0x007F, 0x00FF, 0x01FF, 0x03FF, 0x07FF,
            0x0FFF, 0x1FFF, 0x3FFF, 0x7FFF, 0xFFFF
        ),
        
        bigZero = new BigInt(),
        bigOne = new BigInt();
        
        bigOne.digits[0] = 1;
        
        //PRIVATE METHODES
        var _digitToHex = function(n) {
                var mask = 0xf;
                var result = "";
                for (var i = 0; i < 4; ++i) {
                    result += hexToChar[n & mask];
                    n >>>= 4;
                }
                return _reverseStr(result);
            },
            
            _charToHex = function(c) {
                var ZERO = 48;
                var NINE = ZERO + 9;
                var littleA = 97;
                var littleZ = littleA + 25;
                var bigA = 65;
                var bigZ = 65 + 25;
                var result;

                if (c >= ZERO && c <= NINE) {
                    result = c - ZERO;
                } else if (c >= bigA && c <= bigZ) {
                    result = 10 + c - bigA;
                } else if (c >= littleA && c <= littleZ) {
                    result = 10 + c - littleA;
                } else {
                    result = 0;
                }
                return result;
            },
            
            _hexToDigit = function(s) {
                var result = 0;
                var sl = Math.min(s.length, 4);
                for (var i = 0; i < sl; ++i) {
                    result <<= 4;
                    result |= _charToHex(s.charCodeAt(i))
                }
                return result;
            },
            
            _reverseStr = function(s) {
                var result = "";
                for (var i = s.length - 1; i > -1; --i) {
                    result += s.charAt(i);
                }
                return result;
            },
            
            _arrayCopy = function(src, srcStart, dest, destStart, n) {
                var m = Math.min(srcStart + n, src.length);
                for (var i = srcStart, j = destStart; i < m; ++i, ++j) {
                    dest[j] = src[i];
                }
            },
            
            _setMaxDigits = function(value) {
                var maxDigits = parseInt(value,10);
                bi.ZERO_ARRAY = new Array(maxDigits);
                for (var iza = 0; iza < bi.ZERO_ARRAY.length; iza++){
                   bi.ZERO_ARRAY[iza] = 0;
            };
    
}
        
    //PUBLIC INTERFACE   
    var self = {

        FromDecimal : function(s) {
            var isNeg = s.charAt(0) == '-';
            var i = isNeg ? 1 : 0;
            var result;
            while (i < s.length && s.charAt(i) == '0') ++i;
            if (i == s.length) {
                result = new BigInt();
            }
            else {
                var digitCount = s.length - i;
                var fgl = digitCount % dpl10;
                if (fgl == 0) fgl = dpl10;
                result = self.FromNumber(Number(s.substr(i, fgl)));
                i += fgl;
                while (i < s.length) {
                    result = self.Add(self.Multiply(result, self.FromNumber(maxVal)),
                    self.FromNumber(Number(s.substr(i, dpl10))));
                    i += dpl10;
                }
                result.isNeg = isNeg;
            }
            return result;
        },

        Copy : function(obj) {
            var result = new BigInt(true);
            result.digits = obj.digits.slice(0);
            result.isNeg = obj.isNeg;
            return result;
        },

        FromNumber : function(i) {
            var result = new BigInt();
            result.isNeg = i < 0;
            i = Math.abs(i);
            var j = 0;
            while (i > 0) {
                result.digits[j++] = i & maxDigitVal;
                i >>= bigRadixBits;
            }
            return result;
        },

        ToString : function(x, radix) {
            var b = new BigInt();
            b.digits[0] = radix;
            var qr = self.DivideModulo(x, b);
            var result = hexatrigesimalToChar[qr[1].digits[0]];
            while (self.Compare(qr[0], bigZero) == 1) {
                qr = self.DivideModulo(qr[0], b);
                digit = qr[1].digits[0];
                result += hexatrigesimalToChar[qr[1].digits[0]];
            }
            return (x.isNeg ? "-" : "") + _reverseStr(result);
        },

        ToDecimal : function(x) {
            var b = new BigInt();
            b.digits[0] = 10;
            var qr = self.DivideModulo(x, b);
            var result = String(qr[1].digits[0]);
            while (self.Compare(qr[0], bigZero) == 1) {
                qr = self.DivideModulo(qr[0], b);
                result += String(qr[1].digits[0]);
            }
            return (x.isNeg ? "-" : "") + _reverseStr(result);
        },

        ToHex : function(x) {
            var result = "";
            var n = self.HighIndex(x);
            for (var i = self.HighIndex(x); i > -1; --i) {
                result += _digitToHex(x.digits[i]);
            }
            return result;
        },

        FromHex : function(s) {
            var result = new BigInt();
            var sl = s.length;
            for (var i = sl, j = 0; i > 0; i -= 4, ++j) {
                result.digits[j] = _hexToDigit(s.substr(Math.max(i - 4, 0), Math.min(i, 4)));
            }
            return result;
        },

        FromString : function(s, radix) {
            var isNeg = s.charAt(0) == '-';
            var istop = isNeg ? 1 : 0;
            var result = new BigInt();
            var place = new BigInt();
            place.digits[0] = 1; // radix^0
            for (var i = s.length - 1; i >= istop; i--) {
                var c = s.charCodeAt(i);
                var digit = _charToHex(c);
                var bigDigit = self.MultiplyDigit(place, digit);
                result = self.Add(result, bigDigit);
                place = self.MultiplyDigit(place, radix);
            }
            result.isNeg = isNeg;
            return result;
        },

        Dump : function(b) {
            return (b.isNeg ? "-" : "") + b.digits.join(" ");
        },

        Add : function(x, y) {
            var result;

            if (x.isNeg != y.isNeg) {
                y.isNeg = !y.isNeg;
                result = self.Subtract(x, y);
                y.isNeg = !y.isNeg;
            }
            else {
                result = new BigInt();
                var c = 0;
                var n;
                for (var i = 0; i < x.digits.length; ++i) {
                    n = x.digits[i] + y.digits[i] + c;
                    result.digits[i] = n & 0xffff;
                    c = Number(n >= bigRadix);
                }
                result.isNeg = x.isNeg;
            }
            return result;
        },

        Subtract : function(x, y) {
            var result;
            if (x.isNeg != y.isNeg) {
                y.isNeg = !y.isNeg;
                result = self.Add(x, y);
                y.isNeg = !y.isNeg;
            } else {
                result = new BigInt();
                var n, c;
                c = 0;
                for (var i = 0; i < x.digits.length; ++i) {
                    n = x.digits[i] - y.digits[i] + c;
                    result.digits[i] = n & 0xffff;
                    if (result.digits[i] < 0) result.digits[i] += bigRadix;
                    c = 0 - Number(n < 0);
                }
                if (c == -1) {
                    c = 0;
                    for (var i = 0; i < x.digits.length; ++i) {
                        n = 0 - result.digits[i] + c;
                        result.digits[i] = n & 0xffff;
                        if (result.digits[i] < 0) result.digits[i] += bigRadix;
                        c = 0 - Number(n < 0);
                    }
                    result.isNeg = !x.isNeg;
                } else {
                    result.isNeg = x.isNeg;
                }
            }
            return result;
        },

        HighIndex : function(x) {
            var result = x.digits.length - 1;
            while (result > 0 && x.digits[result] == 0) --result;
            return result;
        },

        NumBits : function(x) {
            var n = self.HighIndex(x);
            var d = x.digits[n];
            var m = (n + 1) * bitsPerDigit;
            var result;
            for (result = m; result > m - bitsPerDigit; --result) {
                if ((d & 0x8000) != 0) break;
                d <<= 1;
            }
            return result;
        },

        Multiply : function(x, y) {
            var result = new BigInt();
            var c;
            var n = self.HighIndex(x);
            var t = self.HighIndex(y);
            var u, uv, k;

            for (var i = 0; i <= t; ++i) {
                c = 0;
                k = i;
                for (var j = 0; j <= n; ++j, ++k) {
                    uv = result.digits[k] + x.digits[j] * y.digits[i] + c;
                    result.digits[k] = uv & maxDigitVal;
                    c = uv >>> bigRadixBits;
                }
                result.digits[i + n + 1] = c;
            }
            result.isNeg = x.isNeg != y.isNeg;
            return result;
        },

        MultiplyDigit : function(x, y) {
            var n, c, uv;

            var result = new BigInt();
            n = self.HighIndex(x);
            c = 0;
            for (var j = 0; j <= n; ++j) {
                uv = result.digits[j] + x.digits[j] * y + c;
                result.digits[j] = uv & maxDigitVal;
                c = uv >>> bigRadixBits;
            }
            result.digits[1 + n] = c;
            return result;
        },
        
        ShiftLeft : function(x, n) {
            var digitCount = Math.floor(n / bitsPerDigit);
            var result = new BigInt();
            _arrayCopy(x.digits, 0, result.digits, digitCount,result.digits.length - digitCount);
            var bits = n % bitsPerDigit;
            var rightBits = bitsPerDigit - bits;
            for (var i = result.digits.length - 1, i1 = i - 1; i > 0; --i, --i1) {
                result.digits[i] = ((result.digits[i] << bits) & maxDigitVal) |
                ((result.digits[i1] & highBitMasks[bits]) >>>
                (rightBits));
            }
            result.digits[0] = ((result.digits[i] << bits) & maxDigitVal);
            result.isNeg = x.isNeg;
            return result;
        },

        ShiftRight : function(x, n) {
            var digitCount = Math.floor(n / bitsPerDigit);
            var result = new BigInt();
            _arrayCopy(x.digits, digitCount, result.digits, 0,x.digits.length - digitCount);
            var bits = n % bitsPerDigit;
            var leftBits = bitsPerDigit - bits;
            for (var i = 0, i1 = i + 1; i < result.digits.length - 1; ++i, ++i1) {
                result.digits[i] = (result.digits[i] >>> bits) |
                ((result.digits[i1] & lowBitMasks[bits]) << leftBits);
            }
            result.digits[result.digits.length - 1] >>>= bits;
            result.isNeg = x.isNeg;
            return result;
        },

        MultiplyByRadixPower : function(x, n) {
            var result = new BigInt();
            _arrayCopy(x.digits, 0, result.digits, n, result.digits.length - n);
            return result;
        },

        DivideByRadixPower : function(x, n){
            var result = new BigInt();
            _arrayCopy(x.digits, n, result.digits, 0, result.digits.length - n);
            return result;
        },

        ModuloByRadixPower : function(x, n){
            var result = new BigInt();
            _arrayCopy(x.digits, 0, result.digits, 0, n);
            return result;
        },

        Compare : function(x, y) {
            if (x.isNeg != y.isNeg) {
                return 1 - 2 * Number(x.isNeg);
            }
            for (var i = x.digits.length - 1; i >= 0; --i) {
                if (x.digits[i] != y.digits[i]) {
                    if (x.isNeg) {
                        return 1 - 2 * Number(x.digits[i] > y.digits[i]);
                    } else {
                        return 1 - 2 * Number(x.digits[i] < y.digits[i]);
                    }
                }
            }
            return 0;
        },

        DivideModulo : function(x, y) {
            var nb = self.NumBits(x);
            var tb = self.NumBits(y);
            var origYIsNeg = y.isNeg;
            var q, r;
            if (nb < tb) {
                if (x.isNeg) {
                    q = self.Copy(bigOne);
                    q.isNeg = !y.isNeg;
                    x.isNeg = false;
                    y.isNeg = false;
                    r = self.Subtract(y, x);
                    x.isNeg = true;
                    y.isNeg = origYIsNeg;
                } else {
                    q = new BigInt();
                    r = self.Copy(x);
                }
                return new Array(q, r);
            }

            q = new BigInt();
            r = x;

            var t = Math.ceil(tb / bitsPerDigit) - 1;
            var lambda = 0;
            while (y.digits[t] < bigHalfRadix) {
                y = self.ShiftLeft(y, 1);
                ++lambda;
                ++tb;
                t = Math.ceil(tb / bitsPerDigit) - 1;
            }

            r = self.ShiftLeft(r, lambda);
            nb += lambda;
            var n = Math.ceil(nb / bitsPerDigit) - 1;

            var b = self.MultiplyByRadixPower(y, n - t);
            while (self.Compare(r, b) != -1) {
                ++q.digits[n - t];
                r = self.Subtract(r, b);
            }
            for (var i = n; i > t; --i) {
                var ri = (i >= r.digits.length) ? 0 : r.digits[i];
                var ri1 = (i - 1 >= r.digits.length) ? 0 : r.digits[i - 1];
                var ri2 = (i - 2 >= r.digits.length) ? 0 : r.digits[i - 2];
                var yt = (t >= y.digits.length) ? 0 : y.digits[t];
                var yt1 = (t - 1 >= y.digits.length) ? 0 : y.digits[t - 1];
                if (ri == yt) {
                    q.digits[i - t - 1] = maxDigitVal;
                } else {
                    q.digits[i - t - 1] = Math.floor((ri * bigRadix + ri1) / yt);
                }

                var c1 = q.digits[i - t - 1] * ((yt * bigRadix) + yt1);
                var c2 = (ri * bigRadixSquared) + ((ri1 * bigRadix) + ri2);
                while (c1 > c2) {
                    --q.digits[i - t - 1];
                    c1 = q.digits[i - t - 1] * ((yt * bigRadix) | yt1);
                    c2 = (ri * bigRadix * bigRadix) + ((ri1 * bigRadix) + ri2);
                }

                b = self.MultiplyByRadixPower(y, i - t - 1);
                r = self.Subtract(r, self.MultiplyDigit(b, q.digits[i - t - 1]));
                if (r.isNeg) {
                    r = self.Add(r, b);
                    --q.digits[i - t - 1];
                }
            }
            r = self.ShiftRight(r, lambda);

            q.isNeg = x.isNeg != origYIsNeg;
            if (x.isNeg) {
                if (origYIsNeg) {
                    q = self.Add(q, bigOne);
                } else {
                    q = self.Subtract(q, bigOne);
                }
                y = self.ShiftRight(y, lambda);
                r = self.Subtract(y, r);
            }

            if (r.digits[0] == 0 && self.HighIndex(r) == 0) r.isNeg = false;

            return new Array(q, r);
        },

        Divide : function(x, y) {
            return self.DivideModulo(x, y)[0];
        },

        Modulo : function(x, y) {
            return self.DivideModulo(x, y)[1];
        },

        MultiplyMod : function(x, y, m) {
            return self.Modulo(self.Multiply(x, y), m);
        },

        Pow : function(x, y) {
            var result = bigOne;
            var a = x;
            while (true) {
                if ((y & 1) != 0) result = self.Multiply(result, a);
                y >>= 1;
                if (y == 0) break;
                a = self.Multiply(a, a);
            }
            return result;
        },

        PowMod : function(x, y, m) {
            var result = bigOne;
            var a = x;
            var k = y;
            while (true) {
                if ((k.digits[0] & 1) != 0) result = self.MultiplyMod(result, a, m);
                k = self.ShiftRight(k, 1);
                if (k.digits[0] == 0 && self.HighIndex(k) == 0) break;
                a = self.MultiplyMod(a, a, m);
            }
            return result;
        },
        
        setMaxDigits : function(value){
            _setMaxDigits(value);
            return bi.ZERO_ARRAY;
        }
        
    };
    return self;
    
})();

//barrett class=================================
function BarrettMu(m) {
	this.modulus = bi.fn.Copy(m);
	this.k = bi.fn.HighIndex(this.modulus) + 1;
	var b2k = new BigInt();
	b2k.digits[2 * this.k] = 1;
	this.mu = bi.fn.Divide(b2k, this.modulus);
	this.bkplus1 = new BigInt();
	this.bkplus1.digits[this.k + 1] = 1;
	
}

BarrettMu.prototype = {

    modulo : function(x) {
        var q1 = bi.fn.DivideByRadixPower(x, this.k - 1);
        var q2 = bi.fn.Multiply(q1, this.mu);
        var q3 = bi.fn.DivideByRadixPower(q2, this.k + 1);
        var r1 = bi.fn.ModuloByRadixPower(x, this.k + 1);
        var r2term = bi.fn.Multiply(q3, this.modulus);
        var r2 = bi.fn.ModuloByRadixPower(r2term, this.k + 1);
        var r = bi.fn.Subtract(r1, r2);
        if (r.isNeg) {
            r = bi.fn.Add(r, this.bkplus1);
        }
        var rgtem = bi.fn.Compare(r, this.modulus) >= 0;
        while (rgtem) {
            r = bi.fn.Subtract(r, this.modulus);
            rgtem = bi.fn.Compare(r, this.modulus) >= 0;
        }
        return r;
    },

    multiplyMod : function(x, y) {
        var xy = bi.fn.Multiply(x, y);
        return this.modulo(xy);
    },

    powMod : function(x, y) {
        var result = new BigInt();
        result.digits[0] = 1;
        while (true) {
            if ((y.digits[0] & 1) != 0) result = this.multiplyMod(result, x);
            y = bi.fn.ShiftRight(y, 1);
            if (y.digits[0] == 0 && bi.fn.HighIndex(y) == 0) break;
            x = this.multiplyMod(x, x);
        }
        return result;
    }
};