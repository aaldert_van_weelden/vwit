<?php

//**********************************************************************************************************
//* Script name: Jsession
//* Purpose: A secure session handling class
//* Author: Justin Silverton
//*         http://www.whenpenguinsattack.com
//*         email: justin@jaslabs.com
//********************************************************************************************************** 

//*****************************************************************************************************************
//* Class Name:  jcart_session
//* Purpose:     session management class
//* Notes:       stores data and session timestamp in a mysql database.  It accomplishes this throug the ez_sql class
//*****************************************************************************************************************
 
include_once BASIS.'include/classes/config.php';          //for all major config variables
include_once BASIS."include/classes/ez_sql_core.php";     //ez_sql core files
include_once BASIS."include/classes/ez_sql_mysql.php";    //ez_sql mysql libraries

class session {

private   $session_variables;         //this is an array that will store all session variables
protected $uniqueid;                  //unique identifier that will identify this person from another
protected $dbhandle;                  //resource handle for the database
private   $errors;                    //this stores any error that occurs during this class, an array of strings
private	  $user_agent;

//*****************************************************************************************************************
//* Function Name:  constructor()
//* Purpose:     gets uniqueid from cookie stored on users system and initializes session
//*****************************************************************************************************************
 
public function __construct() {
 //make this an associative array
 $this->session_variables = array();
 //initialize the mysql connector class
 $this->dbhandle = new ezSQL_mysql(DBUSER,DBPASS,DBNAME,DBHOST);
 $this->user_agent = $_SERVER["HTTP_USER_AGENT"];
 
 //make sure incoming variables are secure
 $this->SecurePosts();
 $this->SecureGets();	
 $this->SecureCookies();//delete invalid session cookies
 
 if (isset($_COOKIE['jcart'])) {
    $this->uniqueid = $_COOKIE['jcart'];
    //get the existing session data
    $sessiondata = $this->dbhandle->get_var("SELECT sessiondata from jcart_sessions where sessionID='$this->uniqueid'");
    //unserialize the data, which converts it from string for back to object representation
    if (isset($sessiondata))
       $this->session_variables = unserialize($sessiondata);
	
	$this->validate_session();
} else {
	$this->uniqueid = $this->GenerateID();
   	//set cookie in client's browser
    $this->setusercookie();
}

//randomly generate a number, for session cleanup.  IF it matches, then check and cleanup all timed out sessions
$generated_checksum = rand(0,1000);

if (SESSIONSEED == $generated_checksum) 
   $this->dbhandle->query("DELETE from jcart_sessions where now()-timestamp > $total_session_time");
}

//*****************************************************************************************************************
//* Function Name:  destructor()
//* Purpose:        writes all variables in session to the database
//*****************************************************************************************************************
 
public function __destruct() {
 //serialize all session data, so it can be stored in the database
 $serialized_data = serialize($this->session_variables);
 
 //if there is a uniqueid set, write all data from session
 if (isset($this->uniqueid)) {
    //find out if the session is already in the database, insert a new session if not, update existing session of yes
	$uniqueresult = $this->dbhandle->get_var("SELECT sessionID from jcart_sessions where sessionID='$this->uniqueid'");
    if (!$uniqueresult)
	   $this->dbhandle->query("INSERT into jcart_sessions set sessionID='$this->uniqueid',sessiondata='$serialized_data',timestamp=now(),user_agent='$this->user_agent' "); 
    else
       $this->dbhandle->query("UPDATE jcart_sessions set sessiondata='$serialized_data', last_impression=now() where sessionID='$this->uniqueid'");
 }

}

//*****************************************************************************************************************
//* Function Name:  setusercookie()
//* Purpose:        writes the cookie to the current user (with timeout set in the config file) 
//*****************************************************************************************************************

protected function setusercookie() {
 //convert the session minute,day,hour,month into seconds, so we can use it with setcookie
 $total_session_time = SESSIONSECOND+(SESSIONMINUTE*60)+(SESSIONHOUR*60*60)+(SESSIONDAY*60*60*24)+(SESSIONMONTH*60*60*24*30);
 
 setcookie('jcart',$this->uniqueid,time()+$total_session_time,'/');	
}

//*****************************************************************************************************************
//* Function Name:  setvar()
//* Purpose:     sets a specific variable in the session
//* Parameters:  $name: variable name
//*              $value: variable value
//*****************************************************************************************************************
 
public function setvar($name,$value) {
  $this->session_variables[$name] = $value;
}

//*****************************************************************************************************************
//* Function Name:  getvar()
//* Purpose:     gets a specific variable in the session
//* Parameters:  $name: variable name
//*****************************************************************************************************************
 
public function getvar($name,$e=false) {
 if (isset($this->session_variables[$name]))
 if ($e)
    echo   $this->session_variables[$name];	
else
	return $this->session_variables[$name];	
}

//*****************************************************************************************************************
//* Function Name:  getID()
//* Purpose:     gets the ID in the session
//* Parameters:  
//*****************************************************************************************************************
 
public function getID($e=false) {
 if (isset($this->uniqueid))
 if ($e)
    echo   $this->uniqueid;
else
    return $this->uniqueid;	
}

//*****************************************************************************************************************
//* Function Name:  impress()
//* Purpose:     updates last_impression in the session for security sessionduration limitation [XHR]
//* Parameters:  
//*****************************************************************************************************************
 
public function impress() {
 if (isset($this->uniqueid)){
	$this->dbhandle->query("UPDATE jcart_sessions set last_impression=now() where sessionID='$this->uniqueid'");
	return $this->uniqueid;
 }
 else {
    return false;	
 }
}

//*****************************************************************************************************************
//* Function Name:  generateID()
//* Purpose:     generates a unique session identifier
//* Notes:       uses the md5 of the current time in seconds+a random number
//*****************************************************************************************************************
 
private function GenerateID() {
 return md5(time()+rand());	
}

//*****************************************************************************************************************
//* Function Name:  validate_session()
//* Purpose:     checks session against user-agent and last-impression
//*****************************************************************************************************************
 
private function validate_session() {

 return true;	
}

//*****************************************************************************************************************
//* Function Name:  generateError()
//* Purpose:     allows this or any inherited add an error message to the list of total errors
//*****************************************************************************************************************
 
protected function GenerateError($errortext) {
 $this->errors = $errortext;
}

//*****************************************************************************************************************
//* Function Name:  getError()
//* Purpose:        gets the last error
//*****************************************************************************************************************
 
protected function GetError($e=false) {
if ($e)
    echo   $this->errors;
else
	return $this->errors;
}
//*****************************************************************************************************************
//* Function Name:  SecureValue()
//* Purpose:     Checks an incoming value for html,length, and mysql validity
//* Parameters:  $value (that needs to be checked), passed by reference
//*****************************************************************************************************************
 
private function SecureValue(&$value) {
 $MaxValueLength = 255;
 
 //make sure the value is limited to 255 bytes
 if (!is_array($value)) { 
    $value = substr($value,0,$MaxValueLength);
    //strip the value of html
    $value = strip_tags($value);
    //secure it for a mysql query (escape special characters)
    $value = $this->dbhandle->escape($value);
 } else {
    foreach ($value as $checkvalue) {
	  $checkvalue = substr($checkvalue,0,$MaxValueLength);
      //strip the value of html
      $checkvalue = strip_tags($checkvalue);
      //secure it for a mysql query (escape special characters)
      $checkvalue = $this->dbhandle->escape($checkvalue);   
    } 
 }
 
}

//*****************************************************************************************************************
//* Function Name:  SecureCookie
//* Purpose:        Checks and cleans all cookies using SecureValue()
//*****************************************************************************************************************
 
private function SecureCookies() {
//get all names of the associative GET array
$get_key_array = array_keys($_COOKIE);
//get the total size of the array so we can iterate through each value
$get_key_size = sizeof($get_key_array);

//cycle through all COOKIE variables and secure them 
for ($count=0;$count<$get_key_size;$count++)
    $this->SecureValue($_COOKIE["$get_key_array[$count]"]);
    //to do : delete invalid session cookies [last impression]
}

//*****************************************************************************************************************
//* Function Name:  SecureGets
//* Purpose:        Checks and cleans all GETS using SecureValue()
//*****************************************************************************************************************
 
private function SecureGets() {
//get all names of the associative GET array
$get_key_array = array_keys($_GET);
//get the total size of the array so we can iterate through each value
$get_key_size = sizeof($get_key_array);

//cycle through all GET variables and secure them 
for ($count=0;$count<$get_key_size;$count++)
    $this->SecureValue($_GET["$get_key_array[$count]"]);
	
	
}

//*****************************************************************************************************************
//* Function Name:  SecurePosts
//* Purpose:        Checks and cleans all POSTS using SecureValue()
//*****************************************************************************************************************
 
private function SecurePosts() {
 //all names of the associative post array
$post_key_array = array_keys($_POST);
//the total size of the array so we can iterate through each value
$post_key_size = sizeof($post_key_array);

//cycle through all POST variables and secure them
for ($count=0;$count<$post_key_size;$count++)
    $this->SecureValue($_POST["$post_key_array[$count]"]);	
}

}
?>