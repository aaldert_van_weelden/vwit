<?php

//database config values
define('DBHOST','localhost');
define('DBUSER','aaldert');
define('DBPASS','bassic');
define('DBNAME','test');
//session timeout config values

//these are the timeout settings for the session
//example usage: sessions that expire after 1 day 30 seconds, S
//SESSIONSECOND=30,SESSIONDAY=1 (the rest set to 0)

define('SESSIONMINUTE',0);
define('SESSIONHOUR',0);
define('SESSIONSECOND',0);
define('SESSIONDAY',30);
define('SESSIONMONTH',0);

define('SESSION_TIMEOUT',15);//max user inactive in minutes

//this is a random number used for expiring sessions in a timely fashion
define('SESSIONSEED',768);
?>