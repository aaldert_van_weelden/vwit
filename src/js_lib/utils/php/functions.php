<?php
	function getPath ($p_iIndex=0, $p_iSelf=false){
		
		$s_path='';
		$a_path=explode("/", $_SERVER['PHP_SELF']);
		
		if($p_iIndex>count($a_path)) {
			$p_iIndex=count($a_path);	
		}
		
		for($t=0;$t<=$p_iIndex;$t++) {
			$t<$p_iIndex?$slash='/':$slash='';
			$s_path .= $a_path[$t].$slash;
	
		}
		
		if($p_iSelf){
			$path=$_SERVER['HTTP_HOST'].$s_path.'/';
			$path=str_replace("//", "/", $path);
			$path='http://'.$path;
			if(substr($path,strlen($path)-1) !='/'){
				$path.='/';
			}
			
		}
		else{
			$path=$_SERVER['DOCUMENT_ROOT'].$s_path.'/';
			$path=str_replace("//", "/", $path);
		}

	return $path;
	}
	
	
/*  USAGE : include at top of the actions.php file :
 *
 *  if(!isset($_GET['action'])){//sla $_GET op als er geen acties ondernomen worden
 *  	setParam();
 *	}
 *
 *  //after isset $_GET['action'] && $_GET['action']=='dosomething' blok :
 *  header("Location: ".getParam(array('action')));
 *	exit;
 */	
function getParam(array $a_Forbidden=array()){
	//voorbeeld : getParam( array ('bestelling','sorteren') );
	global $_SESSION;
	if(is_array($_SESSION['get'])){
		$params='?';
		$lengte=count($_SESSION['get']);
		foreach ($_SESSION['get'] as $name=>$value) {
	
			if(!in_array($name,$a_Forbidden)){
				$params.="$name=$value";
				$params.='&';
			} 
		}
		$params=rtrim($params,"&");
		$params=='?' ? $params='':NULL;
		return $params;
		
	} else {
		return '';
	}
	
	
}
function setParam(){
	global $_SESSION;
	global $_GET;
	$_SESSION['get']=$_GET;
	return true;
}

//needs array $getvars_is_allowed

//testexample
$getvars_is_allowed = array('filteren','page','soort','orderby','sorteren','limit','start','stop','status','p','b','action');
//
function getVars(array $a_Getvars=array()){
    global $getvars_is_allowed;
    $params='?';
    foreach ($getvars_is_allowed as $name){
        if(isset($_GET[$name])){
            if(array_key_exists($name,$a_Getvars)){
                $params.="$name=$a_Getvars[$name]";
            }
            else{
                $params.="$name=$_GET[$name]";
            }
            $params.='&';
        }
        elseif(array_key_exists($name,$a_Getvars)){
            $params.="$name=$a_Getvars[$name]";
            $params.='&';	
        }
    }
    $params=rtrim($params,"&");
	$params=='?' ? $params='':NULL;
	return $params;
    
}

function uri2json($data,$b_JSON=false){
		$data=base64_decode($data);
		$data=urldecode($data);
		if(!$b_JSON){
			return $data;
		}
		else {
			return json_decode($data);
		}
}

function charcodeToTxt($value){
		$result='';
		
        $arr = explode('&', $value);
        foreach($arr as $key=>$val){
        
            $sub=explode(';', $val);
            isset($sub[0])?$one=$sub[0]:$one='';
            isset($sub[1])?$two=$sub[1]:$two='';
            
            if( strstr($one,'#') && strlen($one)>1 ){
                $code=intval( str_replace('#','',$sub[0]) );
                if($code>0){
                    //var_dump($code)
                    $one=chr($code);
                }
            }
            $result.=$one.$two;
        }
		return $result;
};

?>