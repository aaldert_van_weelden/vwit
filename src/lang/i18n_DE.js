var i18n_DE = (function(){
	
	var lang = {
		functionalDescriptionWithoutPlaceholders : 'Dies ist eine Übersetzung ohne Platzhalter ',
		functionalDescriptionWithOnePlaceholder : 'Dies ist eine Übersetzung mit einem Platzhalter : {0}, das ist alles',
		functionalDescriptionWithTwoPlaceholders : 'Dies ist eine Übersetzung mit zwei Platzhaltern. Nummer eins: {0},   und dies ist die Nummer zwei: {1}',
		
		translateInsertValueOne : '--Dieser eingelegt übersetzt subString Nr 1--',
		translateInsertValueTwo : '--Dieser eingelegt übersetzt subString Nr 2--',
		
		dotnotation : 'Dieser Übersetzung ist generiert durch die dot-notation',
	};
	
/* do not edit below this line ---------------------------------------------------------------*/
	return {	
		message : function(key){
			for(name in lang){
				if(name===key){
					return lang[key];	
				}
			}
		},
		
		translations : lang
	};
	
})();