var i18n_EN = (function(){
	
	var lang = {
		functionalDescriptionWithoutPlaceholders : 'This is a translation without placeholders ',
		functionalDescriptionWithOnePlaceholder : 'This is a translation with one placeholder : {0}, thats all',
		functionalDescriptionWithTwoPlaceholders : 'This is a translation with two placeholders. Number one: {0},   and here\'s number two: {1}',
		
		translateInsertValueOne : '--This is an inserted translated substring number one--',
		translateInsertValueTwo : '--This is an inserted translated substring number two--',
		
		dotnotation : 'This is a translation called by dot-notation',
	};
	
/* do not edit below this line ---------------------------------------------------------------*/
	return {	
		message : function(key){
			for(name in lang){
				if(name===key){
					return lang[key];	
				}
			}
		},
		
		translations : lang
		
	};
	
})();