<?php 
/**
 * Copy the assets from the vendor/vwit/foundation/src  directory to the public/assets   directory
 *  
 * @author Aaldert van Weelden
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.1
 * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @return      bool     Returns TRUE on success, FALSE on failure
 * 
 */



class CopyAssets{
	
		
	const SOURCE_ROOT = 0;
	const DESTINATION_ROOT = -3;
	
	public $source;
	public $destination;
	public $count = 0;
	
	public function __construct($source, $destination){
			
		$this->source = $this->getPath(self::SOURCE_ROOT).$source;
		$this->destination = $this->getPath(self::DESTINATION_ROOT).$destination;
	}
		
	public function run(){
		$this->xcopy($this->source, $this->destination);
	}

	private function xcopy($source, $dest){
			// Check for symlinks
		if (is_link($source)) {
			return symlink(readlink($source), $dest);
		}
		
		// Simple copy for a file
		if (is_file($source)) {
			print "\n\tcopy file [$source]";
			$this->count++;
			return copy($source, $dest);
		}
		
		// Make destination directory
		if (!is_dir($dest)) {
			print "\n\tcreating  directory [$dest]";
			mkdir($dest);
		}
		
			// Loop through the folder
		if(!$dir = @opendir($source)) die("\n\n".$source.' is not a directory');
			
		while (false !== $entry = readdir($dir)) {
			// Skip pointers
			if ($entry == '.' || $entry == '..') {
				continue;
			}
		
			// Deep copy directories
			$this->xcopy("$source/$entry", "$dest/$entry");
		}
		
		// Clean up
		closedir($dir);
		return true;
	}
		
	/**
	 * Returns the absolute path to the current level
	 *
	 * @param integer $level The relative level. 0 is the base lavel, use -1, -2 etc to step back
	 * @return string The absolute path
	 */
	private function getPath ($level){
		
		$s_path='';
		$a_path=explode(DIRECTORY_SEPARATOR, __DIR__);
		$depth = count($a_path) + $level;
			
		if($depth>count($a_path)) {
			$depth=count($a_path);
		}
			
		for($t=0;$t<$depth;$t++) {
			$s_path .= $a_path[$t].DIRECTORY_SEPARATOR;
		}
			
		return $s_path;
	}


}

print "\n".'DIRECTORY_SEPARATOR = '.DIRECTORY_SEPARATOR."\n";
print "\ncopying assets...\n";


$argc = $GLOBALS['argc']; //number of arguments passed
$argv = $GLOBALS['argv']; //the arguments passed
		
if(count($argv)<2){
	die('WARNING please provide destination directory relative to application root eg [public'.DIRECTORY_SEPARATOR.'assets]'."\n\n");
}
		
		
$source = "src";
$destination = $argv[1];



$generator = new CopyAssets($source, $destination);
$generator->run();

print "\n Copied $generator->count files to [$generator->destination] successfully\n";

print "\n ---READY---";

?>