<?php
use Utils\StringBuilder;
use Utils\StringConstant;

/**
 * Class used to generate a nice formatted report
 */

class CommandReport extends StringBuilder{
	
	const INFO  = 'Info';
	const WARN  = 'Warn';
	const ERROR = 'Error';
	
	public $level;
	
	/**
	 * Construct the CommandReport instance.
	 * Use this instance to generate a nice human readable report.
	 * Default Report level is CommandReport::INFO
	 * 
	 * @param string $level. Allowed values are CommandReport::INFO, CommandReport::WARN, CommandReport::ERROR,
	 */
	public function __construct($level = self::INFO){
		parent::__construct();
		$this->level = $level;
	}
	
	/**
	 * Add header and footer to the message body
	 *
	 * @param string $header
	 * @param string $footer
	 * @param boolean $nl2br
	 * @return String result
	 */
	public function format($header = null, $footer = null, $nl2br = false){
		if($this->isEmpty()) return StringConstant::EMP;
		
		if($header===null) $header = StringConstant::EOL.'*** start of report ***';
		if($footer===null) $footer = '*** end of report ***';
		$body = $this->toString();
		if($nl2br){
			$body = $this->toString(false, true);
		}
		$out = '';
		$out.=$header;
		$out.=StringConstant::EOL;
		$out.='Level: '.$this->level;
		$out.=StringConstant::EOL;
		$out.=$body;
		$out.=$footer;
		$out.=StringConstant::EOL;
		return $out;
	}
	
	
}

?>