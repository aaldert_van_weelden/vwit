<?php namespace VWIT\Base;

/**
 * Enum Compare modes
 * @author Aaldert van Weelden
 */
class ENUM_CMP{

    /**
     * Compare the enum ordinals
     * @uses compare
     * @var integer
     */
    const ORD = 10;
    /**
     * Compare enum values as integers
     * @uses compare
     * @var integer
     */
    const INT = 20;
    /**
     * Compare enum values lexicographic as strings
     * @uses compare
     * @var integer
     */
    const STR = 30;
}

/**
 * Config parameters
 * @author Aaldert van Weelden
 *
 */
class ENUM_PARAM{
    /**
     * Add the __default -> NULL KV pair
     * @uses __construct
     * @var boolean
     */
    const ADD_DEFAULT_VALUE = true;

    /**
     * Do not add a default NULL value
     * @uses __construct
     * @var boolean
     */
    const NO_DEFAULT_VALUE = false;
}

/**
 * Class to implement enumerations with extended functionality<br>
 * You can map the enumeration keys by overriding Enum::map()<br>
 * You can enable enum translation by using the provided i18n methods
 *
 */

abstract class Enum implements EnumInterface
{
    /**
     * The current selected value, must be public for json serialization
     * @var mixed
     */
    public $value = null;

    /**
     * Classname of the instance, used to revive the enumeration object
     * @var string
     */
    public $clazz = null;

    /**
     * The translated value(s).
     * @var string | array
     */
    public $i18nValue = null;

    /**
     * An array of available constants and values as a list with kv-pairs , must be public to accommodate json serialization
     * @var array
     */
    public $constants = null;


    /**
     * Mapping the enum keys to the desired mapped value<br>
     * Optional, must be specified in the child class if used
     * @var array
     */
    protected static $map;
    
    /**
     * List of translations by name or by value
     * @var array
     */
    private $translations = [];

    /**
     * Constructor
     *
     * @param integer|string $value The value to select
     * @param boolean $default  If ENUM_PARAM::ADD_DEFAULT_VALUE, the enum contains __default = NULL KV-pair , else ENUM_PARAM::NO_DEFAULT_VALUE  (default)<br>
     * If ENUM_PARAM::NO_DEFAULT_VALUE  no __default = NULL is added, when __default=NULL is  not specified as an enum constant then setting the value to NULL will throw an \Exception
     * @throws \Exception
     */
    final public function __construct($value = null, $default = ENUM_PARAM::NO_DEFAULT_VALUE){

        $this->constants = (new \ReflectionClass($this))->getConstants();

        if($default===ENUM_PARAM::ADD_DEFAULT_VALUE && !key_exists('__default', $this->constants)) $this->constants = array_merge(['__default'=>null],$this->constants);
        $this->clazz = get_class($this);

        $this->map();
        $this->checkMapping();
        $this->checkConstants();

        if ($value!==null) {
            $this->set($value);
        } elseif (!in_array($this->value, (array)$this->constants, true)) {
            throw new \Exception("ENUM: No value given in constructor and no default value defined");
        }
    }

    /**
     * Set the mapping if desired by overriding this method in  the child class <br>
     * Must be overridden
     * @return array
     */
    protected function map(){
        self::$map = array();
        return $this->mapping();
    }

    /**
     * Return all available enum key-value pairs
     * @return array
     */
    final public function constants()
    {
        return $this->constants;
    }

    /**
     * Select a new value
     * @param mixed $value
     * @return Enum
     * @throws \\Exception
     */
    final public function set($value){

        if (!in_array($value, (array)$this->constants, true)) {
            throw new \Exception("ENUM::set() Attempt to set unknown value '{$value}'");
        }
        $this->value = $value;
        return $this;
    }

    /**
     * Return the current selected value
     * @return mixed
     */
    final public function value(){
        return $this->value;
    }

    /**
     * Set the external translated value
     * @param string | array $translations
     * @return Enum
     * @throws \Exception when provided translations array is invalid
     */
    final public function setI18n($translations){
        if(is_string($translations)){
            $this->i18nValue = $translations;
            return;
        }
        if(!is_array($translations)) throw new \Exception('ENUM: No valid array with translations provided');
        
		if( !empty(array_diff(array_keys($translations), $this->constants)) ) trigger_error('ENUM: Unnecessary translation entries detected', E_USER_NOTICE);
		if( !empty(array_diff( $this->constants, array_keys($translations)))) trigger_error('ENUM: Missing translations detected', E_USER_NOTICE);
        
        $this->translations = $translations;
        $this->i18nValue = (key_exists($this->value(), $this->translations)?$this->translations[$this->value()]:$this->value());
        return $this;
      
    }

    /**
     * Retrieve the stored translated value
     * @param string | integer $key The translations key
     * @return string $i18nValue  If $key == null (default) then the current enum translation is returned, else the translation for the provided key is treturned if present, else the key is returned
     * @throws \Exception when key is invalid
     */
    final public function i18nValue($key = null){
    	if($key === null){
    		if(empty($this->translations)){
    			if($this->i18nValue === null) return $this->value;
    			return $this->i18nValue;
    		} else {
    			if(key_exists('__default', $this->constants ) ) return $this->i18nValue;
    			return $this->i18nValue;
    			//if($this->value !== null)return $this->i18nValue;
	    		//if(key_exists($key, $this->translations) && key_exists('__default', $this->constants )) return $this->translations[$key];
    		}
    		return $key;
    	}
    	
    	if(!is_string($key) && !is_int($key)) throw new \Exception('ENUM: Provided key must be be of type string or integer');
    	if(!in_array($key, (array)$this->constants, true)){
    		trigger_error('ENUM: Invalid key ['.$key.'] detected', E_USER_NOTICE);
    		return '?['.$key.']?';
    	}
    	
    	if(empty($this->translations)){
    		if($this->i18nValue === null) return $key;
    		if($this->value === $key) return $this->i18nValue;
    		return $key;
    	}
    	
    	if(!isset($this->translations[$key])){
    		if($this->i18nValue === null) return $key;
    		if($this->value === $key) return $this->i18nValue;
    		return $key;
    	} 
    	
    	if($this->i18nValue === null) return $key;
    	if($this->value === $key) return $this->i18nValue;
    	return $this->translations[$key];
    	
    }

    /**
     * Select a new value by constant name
     * @param string $name
     * @return Enum
     * @throws \Exception
     */
    final public function setName($name){
        if (!array_key_exists($name, (array)$this->constants)) {
            throw new \Exception("ENUM: Unknown name  in setName() '{$name}'");
        }
        $this->value = $this->constants[$name];
        return $this;
    }

    /**
     * Get the current selected constant name
     * @return string
     */
    final public function name(){
        return array_search($this->value, (array)$this->constants, true);
    }

    /**
     * Check if the provided enum value and this instance value are equal
     * @param $test Enum|integer|string
     * @param boolean $strict If TRUE comparison is type aware. Default is FALSE
     * @return boolean
     */
    final public function equals($test, $strict = false){
        if($test instanceof Enum)return $strict===true?$test === $this:$test->constants() === $this->constants();
        return $strict===true?$test === $this->value:$test == $this->value;
    }

    /**
     * Compare the enumeration ordinals (list position) by default, or the values
     * Stringvalues are parsed to integer first by default, or compared lexicographical
     * @param Enum $test
     * @param integer $compareMode.  Valid modes:    ENUM_CMP::ORD | ENUM_CMP::INT | ENUM_CMP::STR
     * @return integer|boolean  <br> 0 : equal | 1 : provided enum more then this | -1 : provided enum more then this <br> FALSE if invalid comparison
     */
    final public function compare(Enum $test, $compareMode = ENUM_CMP::ORD){
        switch ($compareMode){
            case ENUM_CMP::ORD:
                $testOrdinal = $test->ordinal();
                $thisOrdinal = $this->ordinal();
                if($testOrdinal===null)$testOrdinal = -1;
                if($thisOrdinal===null)$thisOrdinal= -1;
                if($testOrdinal==$thisOrdinal)return 0;
                if($testOrdinal<$thisOrdinal)return -1;
                if($testOrdinal>$thisOrdinal)return 1;
                break;

            case ENUM_CMP::INT:
                $testValue = $test->value();
                $thisValue = $this->value();
                if($testValue===null)$testValue = -1;
                if($thisValue===null)$thisValue= -1;
                if(intval($testValue)==$thisValue)return 0;
                if(intval($testValue)<$thisValue)return -1;
                if(intval($testValue)>$thisValue)return 1;
                break;

            case ENUM_CMP::STR:
                if($test->value===null || $this->value === null)return false;
                return strcmp( (string) $test->value, (string) $this->value);
                break;
            default:
                throw new \Exception("ENUM::compare(): Invalid compare mode provided");
        }
    }

    /**
     * Return the ordinal, for __default this will be NULL
     * @return NULL|integer
     */
    final public function ordinal(){
        if($this->value === null) return null;
        if(key_exists('__default', $this->constants))return (integer) array_search($this->value, array_values($this->constants), true)-1;
        return (integer) array_search($this->value, array_values($this->constants), true);


    }

    /**
     * Return the enumeration object by its ordinal value, or false if no ordinal can be mapped
     * @param integer $ordinal
     * @return boolean|Enum
     */
    final public function byOrdinal($ordinal){
        if($ordinal===null){
            $this->setName('__default');
            return $this;
        }
        $ordinal = filter_var($ordinal,FILTER_VALIDATE_INT);
        if($ordinal===false)throw new \Exception('Ordinal should be an integer');
        if(key_exists('__default', $this->constants))$ordinal++;
        if(!key_exists($ordinal, array_values($this->constants)))throw new \Exception('Ordinal '.$ordinal.' does not exist');
        $this->set(array_values($this->constants)[(integer) $ordinal]);
        return $this;
    }

    /**
     * Get the current selected constant name
     * @return string
     * @see getName()
     */
    final public function __toString(){
        return (string) $this->name();
    }

    /**
     * Get the current selected value
     * @return mixed
     * @see value()
     */
    final public function __invoke(){
        return $this->value();
    }

    /**
     * Retrieve the enum keys mapping
     * @param mixed $key  The key to fetch the mapped value for, default NULL
     * @return mixed   If a valid key is provided the mapped value is returned. If the key is invalid an empty array is returned. Default the mapping array is returned
     */
    final public function mapping($key = null){
        if($key===null)return self::$map;
        if(key_exists($key, self::$map))return self::$map[$key];
        return [];

    }

    /**
     * Retrieve the mapped value if a mapping is provided
     * @see Enum::map()
     * @return integer|string|null
     * @throws \Exception
     */
    final public function mapped(){
        if(empty(self::$map)){
            throw new \Exception('Enum: No mapping provided, please override Enum::map() in your child enumeration class');
        }
        return self::$map[$this->value()];
    }

    /**
     * Revive the provided stdClass object to the original enumeration
     * @param object $obj
     * @param string $clazz   The class to revive
     */
    final public static function revive($obj, $clazz = null){

        if($obj === null || empty($obj)){
            $error="Enum::revive: provided object to revive cannot be null";
            throw new \Exception($error);
        }
        if(gettype($obj)!="object"){
            $error="Enum::revive: provided object to revive must be of type object, ".gettype($obj)." given";
            throw new \Exception($error);
        }

        if(! property_exists($obj, 'clazz') && empty($clazz)){
            $error = 'Enum::revive: clazz property or argument is missing, probably the object provided  to revive is not a valid Enum descendant';
            throw new \Exception($error);
        }
        
		if(!empty($clazz))$obj->clazz = $clazz;
        if(property_exists($obj, 'clazz'))$revived = new $obj->clazz;
        $revived->set($obj->value);
        
        return $revived;
    }
    /**
     * Check if each enum key is mapped to a specific value.
     * @throws \Exception
     */
    final private function checkMapping(){
        if(!empty(self::$map)){
            if(!is_array(self::$map))throw new \Exception('Enum: Provided mapping must be an array');
            if(count(self::$map) < (key_exists('__default', $this->constants)?count($this->constants)-1:count($this->constants))){
                throw new \Exception('Enum: Incomplete mapping detected');
            }else{
                foreach ($this->constants as $key=>$value){
                    if($key === '__default') continue;
                    if($value === null || is_bool($value) )throw new \Exception('Enum: value cannot be NULL or Boolean');
                    if(!key_exists($value, self::$map))throw new \Exception('Enum: No mapping found for enum value: '.$value);
                }
            }

        }
    }

    /**
     * Check if the enum keys and values are unique
     * @throws \Exception
     */
    final private function checkConstants(){
        $count = count($this->constants);
        if($count !== count(array_unique(array_keys($this->constants))) )throw new \Exception('Enum: Keys are not unique');
        $test= [];
        foreach($this->constants as $key=>$value){
            if(in_array($value, $test,true))throw new \Exception('Enum: Values are not unique');
            $test[] = $value;
        }
        unset($test);
    }


}