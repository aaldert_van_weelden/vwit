<?php namespace VWIT\Base;

interface EnumInterface{

    /**
    * Create and return an instance of the enum with the provided value
    *
    * @param mixed $val Mandatory not NULL when ENUM_PARAM::NO_DEFAULT_VALUE  Optional  when ENUM_PARAM::ADD_DEFAULT_VALUE , default NULL
    * @return Enum
    * 
    * @example
    * public static function get($setValue = null){
    *  	return new MyEnumClass($setValue, ENUM_PARAM::ADD_DEFAULT_VALUE);
    * }
    * 
    */
    public static function get($value = null);
}
