<?php
/**
 * @deprecated  Use VWIT\Base\EnumImpl class  <br>
 * Class to implement enumerations with extended functionality<br>
 * You can map the enumeration keys by overriding Enum::map()<br>
 * You can enable enum translation by using the provided i18n methods
 *
 */
abstract class Enum
{
	/**
	 * The current selected value, must be public for json serialization
	 * @var mixed
	 */
	public $value = null;

	/**
	 * Classname of the instance, used to revive the enmumeration object
	 * @var string
	 */
	public $clazz = null;

	/**
	 * The translated value. The application is responsible for the translations
	 * @var string
	 */
	public $i18nValue = null;

	/**
	 * An array of available constants, must be public for json serialization
	 * @var array
	 */
	public $constants = null;


	/**
	 * Mapping the enum keys to the desired mapped value<br>
	 * Optional, must be specified in the child class if used
	 * @var array
	 */
	protected $map;

	/**
	 * Constructor
	 *
	 * @param integer|string $value The value to select
	 * @param boolean $default  If TRUE, the enum contains __default=NULL KV-pair. Default=TRUE
	 * @throws InvalidArgumentException
	 */
	final public function __construct($value = null, $default = true){

		$this->constants = (new \ReflectionClass($this))->getConstants();

		if($default && !key_exists('__default', $this->constants)) $this->constants = array_merge(['__default'=>null],$this->constants);
		$this->clazz = get_class($this);

		$this->map();
		$this->checkMapping();
		$this->checkConstants();

		if ($value!==null) {
			$this->setValue($value);
		} elseif (!in_array($this->value, (array)$this->constants, true)) {
			throw new \Exception("ENUM: No value given in constructor and no default value defined");
		}
	}

	/**
	 * Set the mapping if desired by overriding this method in  the child class
	 * @return void
	 */
	protected function map(){
		$this->map = array();
	}

	/**
	 * Get all available constants
	 * @return array
	 */
	final public function getConstants()
	{
		return $this->constants;
	}

	/**
	 * Select a new value
	 * @param mixed $value
	 * @throws InvalidArgumentException
	 */
	final public function setValue($value){

		if (!in_array($value, (array)$this->constants, true)) {
			throw new \Exception("ENUM: Unknown value in setValue() '{$value}'");
		}
		$this->value = $value;
	}

	/**
	 * Select a new value
	 * @param mixed $value
	 * @throws InvalidArgumentException
	 */
	final public function set($value){

		if (!in_array($value, (array)$this->constants, true)) {
			throw new \Exception("ENUM: Unknown value in set() '{$value}'");
		}
		$this->value = $value;
	}

	/**
	 * Get the current selected value
	 * @return mixed
	 */
	final public function getValue(){
		return $this->value;
	}

	/**
	 * Set the external translated value
	 * @param string $translation
	 */
	final public function setI18nValue($translation){
		$this->i18nValue = $translation;
	}

	/**
	 * Retrieve the stored translated value
	 * @return string $i18nValue
	 */
	final public function getI18nValue(){
		return $this->i18nValue;
	}

	/**
	 * Select a new value by constant name
	 * @param string $name
	 * @throws InvalidArgumentException
	 */
	final public function setName($name){
		if (!array_key_exists($name, (array)$this->constants)) {
			throw new \Exception("ENUM: Unknown name  in setName() '{$name}'");
		}
		$this->value = $this->constants[$name];
	}

	/**
	 * Get the current selected constant name
	 * @return string
	 */
	final public function getName(){
		return array_search($this->value, (array)$this->constants, true);
	}

	/**
	 * Check if the provided enum value and this instance value are equal
	 * @param Enum|integer|string
	 * @param boolean $byOrdinal If TRUE the ordinals are compared. Default is FALSE
	 * @param boolean $strict If TRUE the objects are compared, if FALSE the set of constants are compared for equality
	 * @return boolean
	 */
	final public function equals($test, $byOrdinal = false, $strict = false){
		if($test instanceof Enum)return $strict===true?$test === $this:$test->getConstants() === $this->getConstants();
		if($byOrdinal===true) return (integer) $test===$this->ordinal();
		return $this->value===$test;
	}

	/**
	 * Compare the enumeration ordinals (list position) by default, or the values if indicated by $byOrdinal = FALSE<br>
	 * Stringvalues are parsed to integer first by default, or compared lexicographical if indicated by $asInteger = FALSE
	 * @param Enum $test
	 * @param boolean $byOrdinal IF TRUE the ordinals are compared. Default is TRUE
	 * @param boolean $asInteger  IF TRUE the values are parsed to an integer value
	 * @return integer 0 : equal | 1 : provided enum more then this | -1 : provided enum more then this
	 */
	final public function compare(Enum $test, $byOrdinal = true, $asInteger = true){
		if($byOrdinal===true){
			$testOrdinal = $test->ordinal();
			$thisOrdinal = $this->ordinal();
			if($testOrdinal===null)$testOrdinal = -1;
			if($thisOrdinal===null)$thisOrdinal= -1;
			if($testOrdinal==$thisOrdinal)return 0;
			if($testOrdinal<$thisOrdinal)return -1;
			if($testOrdinal>$thisOrdinal)return 1;
		}
		if($asInteger===false)return strcmp( (string) $test->value, (string) $this->value);
		$testValue = $test->getValue();
		$thisValue = $this->getValue();
		if($testValue===null)$testValue = -1;
		if($thisValue===null)$thisValue= -1;
		if(intval($testValue)==$thisValue)return 0;
		if(intval($testValue)<$thisValue)return -1;
		if(intval($testValue)>$thisValue)return 1;
	}

	/**
	 * Return the ordinal, for __default this will be NULL
	 * @return NULL|integer
	 */
	final public function ordinal(){
		if($this->value === null) return null;
		if(key_exists('__default', $this->constants))return (integer) array_search($this->value, array_values($this->constants), true)-1;
		return (integer) array_search($this->value, array_values($this->constants), true);


	}

	/**
	 * Return the enumeration object by its ordinal value, or false if no ordinal can be mapped
	 * @param integer $ordinal
	 * @return boolean|Enum
	 */
	final public function byOrdinal($ordinal){
		if($ordinal===null){
			$this->setName('__default');
			return $this;
		}
		$ordinal = filter_var($ordinal,FILTER_VALIDATE_INT);
		if($ordinal===false)throw new \Exception('Ordinal should be an integer');
		if(key_exists('__default', $this->constants))$ordinal++;
		if(!key_exists($ordinal, array_values($this->constants)))throw new \Exception('Ordinal '.$ordinal.' does not exist');
		$this->setValue(array_values($this->constants)[(integer) $ordinal]);
		return $this;
	}

	/**
	 * Get the current selected constant name
	 * @return string
	 * @see getName()
	 */
	final public function __toString(){
		return (string) $this->getName();
	}

	/**
	 * Get the current selected value
	 * @return mixed
	 * @see getValue()
	 */
	final public function __invoke(){
		return $this->getValue();
	}

	/**
	 * Retrieve the enum keys mapping
	 * @return array
	 */
	final public function getMapping(){
		return $this->map;
	}

	/**
	 * Retrieve the mapped value if a mapping is provided
	 * @see Enum::map()
	 * @return integer|string|null
	 */
	final public function mapped(){
		if(empty($this->map)){
			throw new \Exception('Enum: No mapping provided, please override Enum::map() in your child enumeration class');
		}
		return $this->map[$this->getName()];
	}

	/**
	 * Revive the provided stdClass object to the original enumeration
	 * @param stdClass $obj
	 */
	final public static function revive($obj){

		if($obj === null || empty($obj)){
			$error="Enum::revive: provided object to revive cannot be null";
			throw new \Exception($error);
		}
		if(gettype($obj)!="object"){
			$error="Enum::revive: provided object to revive must be of type object, ".gettype($obj)." given";
			throw new \Exception($error);
		}

		if(! property_exists($obj, 'clazz')){
			$error = 'Enum::revive: clazz property is missing, probably the object provided  to revive is not a valid Enum descendant';
			throw new \Exception($error);
		}

		$revived = new $obj->clazz;
		$revived->setValue($obj->value);
		return $revived;
	}
	/**
	 * Check if each enum key is mapped to a specific value.
	 * @throws \Exception
	 */
	final private function checkMapping(){
		if(!empty($this->map)){
			if(!is_array($this->map))throw new \Exception('Enum: Provided mapping must be an array');
			if(count($this->map) < (key_exists('__default', $this->constants)?count($this->constants)-1:count($this->constants))){
				throw new \Exception('Enum: Incomplete mapping detected');
			}else{
				foreach ($this->constants as $key=>$value){
					if($key === '__default') continue;
					if(!key_exists($key, $this->map))throw new \Exception('Enum: No mapping found for enum key: '.$key);
				}
			}

		}
	}

	/**
	 * Check if the enum keys and values are unique
	 * @throws \Exception
	 */
	final private function checkConstants(){
		$count = count($this->constants);
		if($count !== count(array_unique(array_keys($this->constants))) )throw new \Exception('Enum: Keys are not unique');
		$test= [];
		foreach($this->constants as $key=>$value){
			if(in_array($value, $test,true))throw new \Exception('Enum: Values are not unique');
			$test[] = $value;
		}
		unset($test);
	}


}