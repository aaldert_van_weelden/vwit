<?php namespace VWIT\Base;

/**
 * Use this class to bind events
 * @author aaldert.vanweelden
 *
 * <pre>
 * Example 1:
 * Event::bind('blog.post.create', function($args = array())
 * {
 *   mail('myself@me.com', 'Blog Post Published', $args['name'] . ' has been published');
 * });
 *
 * Example 2:
 * Event::trigger('blog.post.create', $postInfo);
 * </pre>
 *
 */
class Event {

	public static $events = [];

	public static function trigger($event, $args = array()) {
		if (isset(self::$events[$event])) {

			foreach (self::$events[$event] as $func) {

				call_user_func($func, $args);
			}
		}
	}

	public static function bind($event, \Closure $func) {
		self::$events[$event][] = $func;
	}
}
