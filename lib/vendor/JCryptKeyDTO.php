<?php namespace ThirdParty;

use DTO\DTO;

class JCryptKeyDTO extends DTO{
	
	const ONE_K = 1024;
	const TWO_K = 2048;
	
	public $e;
	public $d;
	public $n;
	public $publickeys;
	
	/**
	 * Constructor
	 * Generates and stores the keys
	 */
	public function __construct($keyLength = self::ONE_K){

		$jCryption = new JCryption ();
		
		$keys = (object) $jCryption->generateKeypair ( $keyLength );
		
		$this->e = (object) [
				"int" => $keys->e,
				"hex" => $jCryption->dec2string ( $keys->e, 16 )
		];
		$this->d = (object) [
				"int" => $keys->d,
				"hex" => $jCryption->dec2string ( $keys->d, 16 )
		];
		$this->n = (object) [
				"int" => $keys->n,
				"hex" => $jCryption->dec2string ( $keys->n, 16 )
		];
		
		$this->publickeys = [
				'e' 		=> $this->e->hex,
				'n' 		=> $this->n->hex,
				'maxdigits' => intval ( $keyLength * 2 / 16 + 3 )
		];
		
	}
	
}