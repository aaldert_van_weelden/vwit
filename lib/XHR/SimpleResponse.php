<?php namespace XHR;
use  Utils\Util as Util;
use XHR\XhrControl as XhrControl;
use Utils\DateTimeUtil;
/**
 * class used as XHR response object to wrap data and convert it to JSON string
 * @author Aaldert van Weelden
 *
 */

class SimpleResponse extends JSON{

	private /*String*/ $id;
	private /*Object*/ $data;
	
	public /*String*/ $clazz;
	public /*String*/ $timestamp;
	
	/**
	 * Constructor
	 * 
	 * @param String $name
	 * @param String $description
	 * @param String $success
	 * @param String $clazz
	 */
	public function __construct($clazz=null){
		parent::__construct();
		
		$this->timestamp = DateTimeUtil::getNowGMT();
		$this->id = Util::create_GUID();
		$this->data = new \StdClass;
		$this->clazz = $clazz;
	}
	
	public function getID(){
		return $this->id;
	}
	
	public function setID($id){
		$this->id=$id;
	}
	
	public function getData(){
		return $this->data;
	}
	
	public function setData($data){
		$this->data=$data;
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
	
	
}

?>
