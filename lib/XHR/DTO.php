<?php  namespace DTO;

use \Reflectable;
use \Utils\Util;
/**
 * The dto acts like a data transport object. This is a generic DTO class, can be used as a universal container
 *
 * @author Aaldert van Weelden
 *
 */
abstract class DTO extends Reflectable{

	const AS_CSV = true;
	const AS_ARRAY = false;

	public function __construct(){

	}

	/* (non-PHPdoc)
	 * @see Reflectable::requestToDTO()
	 */
	public function requestToDTO($request, $ignore = null, $map = null){
		if(Util::nullOrEmpty($request)){
			$error="provided DTO object cannot be null";
			throw new \NullArgumentException($error);
		}
		if(! is_object($request) ){
			$error="provided DTO object must be of type object, ".gettype($request)." given";
			throw new \InvalidTypeException($error);
		}
		if(Util::notNullOrEmpty($ignore) && is_array($ignore)){
			foreach ($ignore as $prop){
				unset($request->{$prop});
			}
		}

		$dtoProperties = $this->getProperties();
		$requestProperties= array_keys(get_object_vars($request));

		if($map!==null){
			//map the request keys
			foreach ($requestProperties as $key){
				if(key_exists($key, $map)){
					$request->{$map[$key]} = $request->{$key};
					unset($request->{$key});
				}
			}
			$requestProperties= array_keys(get_object_vars($request));
		}

		$unsupported = array_diff($requestProperties,$dtoProperties);

		if(Util::notNullOrEmpty($unsupported)){
			$error = "Unsupported DTO fields detected: ".implode(',',$unsupported);
			throw new \PropertyException($error);
		}

		foreach($dtoProperties as $key=>$property){
			if(in_array($property,$requestProperties)){
				$this->{$property} = $request->{$property};
			}
		}

		return $this;

	}

	/**
	* Propagate the DTO properties and values to the $_POST array
	* @param boolean $initPost If TRUE the $_POST array is initialized to an empty array
	* @return object The DTO object
	*/
	public function DTOtoRequest($initPost=true){

		if($initPost) $_POST = array();

		$dtoProperties = $this->getProperties();

		foreach($dtoProperties as $key=>$property){
			$_POST[$property] = $this->{$property};
		}

		return $this;
	}

/* (non-PHPdoc)
	 * @see Reflectable::equals()
	 */
	public function equals($obj){

		$dtoProperties = array();
		$objProperties = array();

		try {

			$dtoPropertyNames = (array) (new \ReflectionClass($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
			$objPropertyNames = (array) (new \ReflectionClass($obj))->getProperties(\ReflectionProperty::IS_PUBLIC);

			foreach($dtoPropertyNames as $prop){
				$dtoProperties[$prop->name]= $this->{$prop->name};
			}
			foreach($objPropertyNames as $prop){
				$objProperties[$prop->name]= $obj->{$prop->name};
			}
		}
		catch (\Exception $e) {
			return false;
		}

		if(    md5(implode('#',$dtoProperties)) === md5(implode('#',$objProperties))   ){
			return true;
		}
		return false;
	}


	/* (non-PHPdoc)
	 * @see Reflectable::find()
	 */
	public function findIn($key, $value){
		//TODO implement this
		//$this->dto->findIn('bijlagedata.bestandsnaam', 'OSO-dossier.xml')->bestand
		if($key==null || $value ==null){
			return null;
		}

		$root = null;
		$child = null;

		$propNames = explode(".", $key);
		if(count($propNames>0)){
			$root = $propNames[0];
		}

		if(count($propNames>1)){
			$child = $propNames[1];
		}

		if(is_array($this->{$root})){
			foreach ($this->{$root} as $item){
				if(in_array($value, $item)){
					return (object) $item;
				}
			}
		}
		return null;
	}


	public function toArray() {
		return array_merge(get_object_vars($this));

	}


	/**
	 * Retrieve the list of DTO fieldnames as array (default) or as CSV
	 * @param boolean $csv
	 * @param array $ignore
	 * @return string|array
	 */
	public function getFields($csv = self::AS_ARRAY, $ignore = []){

		$dtoPropertyNames = $this->getProperties();
		$ignore[] = 'clazz';
		$dtoPropertyNames= array_diff( $dtoPropertyNames, $ignore);

		if($csv){
			return implode(",", $dtoPropertyNames);
		}
		return $dtoPropertyNames;
	}

	/**
	 * Return the DTO values
	 * @param boolean $csv
	 * @param array $ignore
	 * @return string|string[]|NULL[]
	 */
	public function getValues($csv = self::AS_ARRAY, $ignore = []){
		$dtoProperties = [];
		$dtoPropertyNames = $this->getProperties();
		$ignore[] = 'clazz';
		$dtoPropertyNames= array_diff( $dtoPropertyNames, $ignore);

		foreach($dtoPropertyNames as $key=>$property){
			switch(gettype( $this->{$property} ) ){
				case 'string':
					$dtoProperties[] = "'".$this->{$property}."'";
					break;
				case 'integer':
					$dtoProperties[] = $this->{$property};
					break;
				default:
					$dtoProperties[] = "'".$this->{$property}."'";
					break;
			}
		}

		if($csv){
			return implode(",", $dtoProperties);
		}
		return $dtoProperties;

	}

	/**
	 * Return the content of the DTO as key/value pairs
	 * @param boolean $csv
	 * @param array $ignore The fields to be ignored
	 * @return string|string[]|NULL[]
	 */
	public function getKVpairs($csv = self::AS_ARRAY, $ignore = ['clazz']){
		$dtoProperties = [];
		$dtoPropertyNames = $this->getProperties();
		$dtoPropertyNames= array_diff( $dtoPropertyNames, $ignore);

		foreach($dtoPropertyNames as $key=>$property){
			switch(gettype( $this->{$property} ) ){
				case 'string':
					$dtoProperties[$property] = "'".$this->{$property}."'";
					break;
				case 'integer':
					$dtoProperties[$property] = $this->{$property};
					break;
				default:
					$dtoProperties[$property] = "'".$this->{$property}."'";
					break;
			}
		}
		if($csv){
			$r = [];
			foreach ($dtoProperties as $key=>$val){
				$r[] = '`'.$key.'` = '.$val;
			}
			return implode(",", $r);
		}
		return $dtoProperties;

	}

	/**
	 * Return the properties an values as CSV KV-pairs
	 * @return string
	 */
	public function __toString(){
		return $this->getKVpairs(self::AS_CSV, []);
	}
}




?>