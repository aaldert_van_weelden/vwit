<?php
/**
 * Enum to describe the allowed HTTP methods for the Curl helper
 */

use VWIT\Base\ENUM_PARAM;

class CurlMethod extends Enum{
	
	
	const GET = 'GET';
	const POST = 'POST';
	const PUT = 'POST';
	const DELETE = 'POST';
	const PATCH = 'PATCH';
	
	//default value
	public $value = self::GET;
	
	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE){
		return new CurlMethod($value, $default);
	}
	
}