<?php
use Utils\Util;
use DTO\DTO;
use XHR\JSON;
use VWIT\Base\Enum;

/**
 * Base class for the ESB message types
 * @author Aaldert van Weelden
 *
 */
abstract class CurlMessage extends JSON{
	
	const SUCCESS = true;
	const FAILURE = false;
	
	/**
	 * Property validation
	 * The provided properties cannot be NULL.
	 * Used for validation
	 * @see CurlMessage::validate() method
	 * @var array
	 */
	private $notNullable;
	
	/**
	 * The message delivery success status,  Message::SUCCESS || Message::FAILURE
	 * @var boolean constant
	 */
	protected $success;
	/**
	 * The error messages  or NULL
	 * @var string
	 */
	protected $error;
	
	/**
	 * The message report container
	 * @var CurlReport
	 */
	protected $report;
	
	/**
	 * The Message Constructor. Creates the new message UUID and initiates the JSON headers
	 * @see XHR\JSON
	 * @param array $notNullable The array used for validation. Properties are not allowed to be NULL
	 * @see CurlMessage::validate() method
	 * @param boolean $success
	 */
	public function __construct(array $notNullable, $success){
		parent::__construct();
		$this->notNullable = $notNullable;
		$this->success = $success;
		$this->report = new CurlReport();
	}
	
	/**
	 * Retrieve the internal persistence message ID. This is usually a generated UUID.
	 * @see Utils\Util::createUUID() method
	 * @return string  The UUID
	 */
	public abstract function getID();
	
	/**
	 * Set the internal persistence message ID. This is usually a generated UUID.
	 * @see Utils\Util::createUUID() method
	 * @param string $id The UUID
	 */
	public abstract function setID($id);
	
	/**
	 * Get the report object
	 * @return CurlReport
	 */
	public  function getReport(){
		return $this->report;
	}
	
	/**
	 * Check if the message has errors and failed
	 * @return boolean TRUE on failure
	 */
	public function hasFailed(){
		return !$this->report->error->isEmpty();
	}
	
	/**
	 * Get the success status
	 * @return boolean
	 */
	public function isSuccess(){
		return $this->success;
	}
	/**
	 * Set the success status.
	 * @param boolean $success
	 */
	public function setSuccess($success){
		$this->success=$success;
	}
	
	/**
	 * Get the custom error messages
	 */
	public function getError(){
		return $this->error;
	}
	
	/**
	 * Set the custom error messages
	 * @param String $error
	 */
	public function setError($error){
		$this->error=$error;
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
	
	/**
	 * Revive the original message from the request object
	 * 
	 * Ignored properties are clazz, created_at and updated_at
	 * 
	 * @param array|object $request  The request array|object
	 * @return CurlMessage $message  The revived message
	 */
	public function revive($request){
		
		if(Util::nullOrEmpty($request)){
			$error="NullArgumentException: provided Message object cannot be null.";
			$this->report->error->append($error);
			return $this;
		}
		
		if( ! (is_object($request) || is_array($request)) ){
			$error="InvalidTypeException: provided Message object must be of type object or array, ".gettype($request)." given";
			$this->report->error->append($error);
			return $this;
		}
		
		$request = (object) $request;
		
		if(property_exists($request, 'clazz')) unset($request->clazz);
		if(property_exists($request, 'notNullable')) unset($request->notNullable);
		if(property_exists($request, 'created_at')) unset($request->created_at);
		if(property_exists($request, 'updated_at')) unset($request->updated_at);
		
		$dtoProperties = $this->getProperties();
		$requestProperties= array_keys(get_object_vars($request));
		
		$unsupported = array_diff($requestProperties,$dtoProperties);
		
		if(Util::notNullOrEmpty($unsupported)){
			$error = "PropertyException: Unsupported Message fields detected: ".implode(',',$unsupported);
			$this->report->error->append($error);
			return $this;
		}
		
		foreach($dtoProperties as $key=>$property){
			if(in_array($property,$requestProperties)){
				$this->{$property} = $request->{$property};
			}
		}
		
		//revive the payload
		if(property_exists($request, 'payload') && Util::notNullOrEmptyOrZero($request->payload)&& is_array($request->payload) ){
			
			$payload = (object) $request->payload;
			
			if(property_exists($payload, 'clazz') && Util::notNullOrEmptyOrZero($payload->clazz)){
				
				$classname = 'Esb\Connector\Payload\\'.$payload->clazz;
				$dto =  new $classname();
				
				$payloadProps= array_keys(get_object_vars($payload));
				
				foreach($payloadProps as $key=>$property){
					if(in_array($property,$payloadProps)){
						$dto->{$property} = $payload->{$property};
					}
				}
				
				$this->payload = $dto;
			}else{
				$this->payload = $payload;
			}	
		}
		
		//revive the status
		if(		property_exists($request, 'status') 
				&& Util::notNullOrEmptyOrZero($request->status)
				&& is_array($request->status)
		){
			$this->status = Enum::revive( (object) $request->status);
			$this->status->translate();
		}
		
		return $this;	
	}
	
	/**
	 * Validate the current message
	 * 
	 * @param array $warnIfNull  The properties as array for which to warn if the content is NULL
	 * @return CurlMessageValidation  The validation report
	 */
	public function validate($warnIfNull=[]){
		
		$validationReport = new CurlMessageValidation();
		
		foreach($this->getProperties() as $name){
			
			if(Util::nullOrEmpty( $this->{$name}) ){
				if(in_array($name, $this->notNullable)){
					
					$validationReport->valid = false;
					$error = "validate::property[$name] cannot be NULL or empty";
					$validationReport->errorReport->appendEOL($error);
				}else{
					
					if(in_array($name, $warnIfNull)){
						$warn = "validate::property[$name] is NULL or empty";
						$validationReport->warnReport->appendEOL($warn);
					}
				}
			}
		}
		
		return $validationReport;
	}
	
	
	private function getProperties(){
		
		$properties = array();
		$pairs = array();
		$reflect = new ReflectionClass($this);
		$props = (array) $reflect->getProperties(ReflectionProperty::IS_PROTECTED);
		
		foreach($props as $prop){
			$properties[]=$prop->name;
		}
		return $properties;
	}
}