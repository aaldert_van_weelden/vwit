<?php
use Utils\StringBuilder;

class CurlReport{
	
	/**
	 *
	 * @var StringBuilder
	 */
	public $success;
	
	/**
	 * 
	 * @var StringBuilder
	 */
	public $info;
	
	/**
	 *
	 * @var StringBuilder
	 */
	public $warning;
	
	/**
	 *
	 * @var StringBuilder
	 */
	public $error;
	
	public function __construct(){
		$this->success 	= new StringBuilder();
		$this->info 	= new StringBuilder();
		$this->warning 	= new StringBuilder();
		$this->error 	= new StringBuilder();
	}
	
	/**
	 * @Override
	 * @return string
	 */
	public function __toString(){
		$out = '';
		if(!$this->success->isEmpty()) $out.=' ||SUCCESS: '.$this->success;
		if(!$this->info->isEmpty()) $out.=' ||INFO: '.$this->info;
		if(!$this->warning->isEmpty()) $out.=' ||WARNING: '.$this->warning;
		if(!$this->error->isEmpty()) $out.=' ||ERROR: '.$this->error;
		return $out;	
	}
	
	/**
	 * Get the raw resultsets
	 * @return array
	 */
	public function raw(){
		
		return [
			'success' 	=> $this->success->raw(),
			'info' 		=> $this->info->raw(),
			'warning' 	=> $this->warning->raw(),
			'error' 	=> $this->error->raw(),
		];
	}
}