<?php
use Utils\StringBuilder;

class CurlMessageValidation{
	
	public $valid;
	public $infoReport;
	public $warnReport;
	public $errorReport;
	
	
	
	public function __construct(){
		$this->valid = true;
		$this->infoReport = new StringBuilder();
		$this->warnReport = new StringBuilder();
		$this->errorReport = new StringBuilder();
	}
	
	

	public function isValid(){
		return $this->valid;
	}
	
	
	public function getInfoReport(){
		return $this->infoReport;
	}
		
	
	public function getWarnReport(){
		return $this->warnReport;
	}
	
	
	public function getErrorReport(){
		return $this->errorReport;
	}

}