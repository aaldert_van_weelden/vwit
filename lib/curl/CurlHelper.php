<?php
/**
 * CurlHelper contains methods to send messages over HTTP and handle the authentication
 * The class sends JSON objects and expects JSON objects in return
 */

class CurlHelper{
	/**
	 * Enable the use of the Fiddle proxy  to inspect cUrl traffic on localhost
	 * @var boolean
	 */
	const ENABLE_FIDDLER_PROXY = true;
	/**
	 * Disable the use of the Fiddle proxy  to inspect cUrl traffic on localhost
	 * @var boolean
	 */
	const DISABLE_FIDDLER_PROXY = false;
	
	private $auth;
	private $httpMethod;
	private $ch;
	private $url;
	private $response;
	private $message;
	private $useProxy;
	
	
	/**
	 * Constructor, used to set generic configuration on initialisation
	 * 
	 * @param CurlMethod $httpMethod
	 * @param string $url The destination URI
	 * @param CurlMessage $message  The message to dispatch
	 * @param CurlAuth $curlAuth  The Basic Auth wrapper
	 * @param boolean $useProxy  CurlHelper::ENABLE_FIDDLER_PROXY ||  CurlHelper::DISABLE_FIDDLER_PROXY (default)
	 */
	public function __construct($httpMethod, $url, CurlMessage $message = null, CurlAuth $curlAuth = null, $useProxy = self::DISABLE_FIDDLER_PROXY){
		$this->auth = $curlAuth;
		$this->httpMethod = $httpMethod;
		$this->ch = curl_init();
		$this->message = $message;
		$this->url = $url;
		$this->useProxy = $useProxy;
	}
	
	/**
	 * Execute the request
	 */
	public function execute(){
		
		/*
		 * TEST ONLY send traffic through fiddler for inspection on localhost
		 */
		if($this->useProxy){
			curl_setopt($this->ch, CURLOPT_PROXY, '127.0.0.1:8888');
		}
		
		if($this->auth !== null){
			curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($this->ch, CURLOPT_USERPWD, $this->auth->applicationKey.":".$this->auth->applicationToken);
		}
		
		switch($this->httpMethod){
			case CurlMethod::GET:
				$this->url .= '/getStatus/'.$this->message->getID();
				curl_setopt($this->ch, CURLOPT_URL, $this->url );
				break;
				
			case CurlMethod::POST:
				
				$data_string = $this->message->encodeJSON();
				
				curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, CurlMethod::POST);
				curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				curl_setopt($this->ch, CURLOPT_URL, $this->url );
				break;
			
		}
		
		
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);//IMP if the url has https and you don't want to verify source certificate
			
		curl_setopt($this->ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 5);
		
		//execute post
		$this->response = curl_exec($this->ch);
		
		//close connection
		curl_close($this->ch);
		
		
	}
	
	/**
	 * Get the response as JSON string
	 * @return string
	 */
	public  function getResponse(){
		return $this->response;
	}
	
	/**
	 * Get the response as an object
	 * @return object
	 */
	public  function getResponseObject(){
		return json_decode( $this->response );
	}
	

}