<?php
/**
 * Class used to encapsulate the Curl authentication properties
 */

class CurlAuth{
	
	public function __construct($applicationKey, $applicationToken){
		$this->applicationKey = $applicationKey;
		$this->applicationToken = $applicationToken;
	}

	public $user;
	public $password;
	
	public $applicationKey;
	public $applicationToken;
	
	
}