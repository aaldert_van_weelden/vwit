<?php
use Utils\DateTimeUtil;
use XHR\XhrResponse;
use XHR\HTTPcodes;
/**
 * Class used for creating different types of HTTP responses
 */


class XhrHelper{
	
	const JSONENCODE = true;
	const PLAINTXT = false;

	/**
	 * Returns a valid JSON  response
	 * @param XHR\XhrResponse $xhr
	 * @param integer $responseCode;
	 * @return \Illuminate\Http\Response
	 * @see XHR\HTTPcodes
	 */
	public static function sendJsonResponse($xhr, $responseCode = null){

		$content = $xhr->encodeJSON();
		
		if($responseCode===null) {
			$responseCode=HTTPcodes::HTTP_OK;
		}
		
		$response = Response::make($content, $responseCode);
		
		$response->header('Cache-Control','no-cache, must-revalidate');
		$response->header('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
		$response->header('Content-type', 'application/json');
		$response->header('Content-Length', strlen($content));
		$response->header('Date', DateTimeUtil::getNowGMT(DateTime::COOKIE));
			
		return $response;
	}
	
	
	/**
	 * Returns a valid JSON  response
	 * @param string $body
	 * @param integer $responseCode;
	 * @return \Illuminate\Http\Response
	 * @see XHR\HTTPcodes
	 */
	public static function sendJson($body, $responseCode = null, $mode = self::PLAINTXT){
	
		if($mode===self::JSONENCODE){
			$body = json_encode($body);
		}
		
	
		if($responseCode===null) {
			$responseCode=HTTPcodes::HTTP_OK;
		}
	
		$response = Response::make($body, $responseCode);
	
		$response->header('Cache-Control','no-cache, must-revalidate');
		$response->header('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
		$response->header('Content-type', 'application/json');
		$response->header('Content-Length', strlen($body));
		$response->header('Date', DateTimeUtil::getNowGMT(DateTime::COOKIE));
			
		return $response;
	}

}

?>