<?php
use Utils\DateTimeUtil;
use XHR\XhrResponse;
use XHR\HTTPcodes;

/**
 * Class used for pretty print the ExceptionString and creating custom SOAP exceptions
 */


class ExceptionHelper{
	
	public static function toHTML($e){
		return '<pre>'.$e->__toString().'</pre>';
	}
	
	/**
	 * Return a simple SOAP error response
	 * @return \Illuminate\Http\Response
	 */
	public static function permissionDeniedSoapResponse(){
	
		$content = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>Permission denied</faultstring></soap:Fault></soap:Body></soap:Envelope>";
			
		$response = Response::make($content, HTTPcodes::HTTP_FORBIDDEN);
			
		$response->header('Content-Encoding', 'gzip');
		$response->header('X-FRAME-OPTIONS', 'SAMEORIGIN');
		$response->header('Date', DateTimeUtil::getNowGMT(DateTime::COOKIE));
		$response->header('Vary', 'Accept-Encoding');
		$response->header('Content-Type', 'text/xml;charset=UTF-8');
		$response->header('Content-Length', strlen($content));
		$response->header('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
			
		return $response;
	}
	
	/**
	 * Return a SOAP error response when an exception is thrown
	 * @param Exception $e
	 * @return \Illuminate\Http\Response
	 */
	public static function exceptionSoapResponse($e, $responseCode = null){
	
		$content = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>".$e->getMessage()."</faultstring><detail>".$e->getCode()."</detail></soap:Fault></soap:Body></soap:Envelope>";

		if(empty($responseCode)){
			$responseCode = HTTPcodes::HTTP_INTERNAL_SERVER_ERROR;
		}
		$response = Response::make($content, $responseCode);
			
		$response->header('Content-Encoding', 'gzip');
		$response->header('X-FRAME-OPTIONS', 'SAMEORIGIN');
		$response->header('Date', DateTimeUtil::getNowGMT(DateTime::COOKIE));
		$response->header('Vary', 'Accept-Encoding');
		$response->header('Content-Type', 'text/xml;charset=UTF-8');
		$response->header('Content-Length', strlen($content));
		$response->header('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
			
		return $response;
	}
	
	/**
	 * Return a JSON error response when an exception is thrown
	 * @param Exception $e
	 * @param integer $responseCode  The HTTP response code
	 * @return \Illuminate\Http\Response
	 */
	public static function exceptionJsonResponse($e, $responseCode = null){
	
		$json = new XhrResponse('Exception','Internal Server Error',false);
		unset($json->controls);
		unset($json->assets);
		unset($json->clazz);
		$json->setData(null);
		$json->setMessage(null);
		if(class_exists('EnvironmentKey') && isset($_ENV[EnvironmentKey::SEND_VERBOSE_EXCEPTIONS]) && $_ENV[EnvironmentKey::SEND_VERBOSE_EXCEPTIONS]){
			$json->setError($e->__toString());
		}else{
			$json->setError($e->getMessage());
		}
		
		$json->setCode($e->getCode());
		
		$content = $json->encodeJSON();
		
		if(empty($responseCode)){
			$responseCode = HTTPcodes::HTTP_INTERNAL_SERVER_ERROR;
		}
		$response = Response::make($content, $responseCode);
		
		$response->header('Cache-Control','no-cache, must-revalidate');
		$response->header('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
		$response->header('Content-type', 'application/json');	
		$response->header('Date', DateTimeUtil::getNowGMT(DateTime::COOKIE));
		$response->header('Vary', 'Accept-Encoding');
		$response->header('Content-Length', strlen($content));
			
		return $response;
	}
	
	
	/**
	 * Sends a JSON error response when an exception is thrown.
	 * Uses $_ENV[EnvironmentKey::SEND_VERBOSE_EXCEPTIONS] = true/false to enable/disable verbose exception messages
	 * @param Exception $e
	 * @param integer $responseCode  The HTTP response code
	 * @return \Illuminate\Http\Response
	 */
	public static function sendExceptionJsonResponse($e, $responseCode = null){
	
		$json = new XhrResponse('Exception','Internal Server Error',false);
		unset($json->controls);
		unset($json->assets);
		unset($json->clazz);
		$json->setData(null);
		$json->setMessage(null);
		
		if(class_exists('EnvironmentKey') && isset($_ENV[EnvironmentKey::SEND_VERBOSE_EXCEPTIONS]) && $_ENV[EnvironmentKey::SEND_VERBOSE_EXCEPTIONS]){
			$json->setError($e->__toString());
		}else{
			$json->setError($e->getMessage());
		}
	
		$json->setCode($e->getCode());
	
		if(empty($responseCode)){
			$responseCode = HTTPcodes::HTTP_INTERNAL_SERVER_ERROR;
		}
		
		$json->sendJSON( false,$responseCode);
	}
	
}

?>