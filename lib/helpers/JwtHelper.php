<?php
use Utils\Util;
use Firebase\JWT\JWT;

/**
 * Class used to encapsulate static JWT helper methods
 */
class JwtHelper{
	
	// allowed algorithms
	
    const HS256 = 'HS256';
    const HS384 = 'HS384';
    const HS512 = 'HS512';
    const RS256 = 'RS256';
    const RS384 = 'RS384';
    const RS512 = 'RS512';
    const ES256 = 'ES256';
    const ES384 = 'ES384';
	
	/**
	 * 
	 * @param string $secret  Base64 encoded binary string
	 * Best suggestion is the key to be a binary string and
	 * store it in encoded in a config file.
	 *
	 * Can be generated with base64_encode(openssl_random_pseudo_bytes(64));
	 * keep it secure! You'll need the exact key to verify the
	 * token later.
	 * 
	 * @param string $algorithm  See the JwtHelper constants
	 * @param array $data  The data to send
	 * @param integer $notBeforeOffset  The offset in seconds
	 * @param integer $expireOffset The offset in seconds
	 * @param string $serverName  Default is localhost
	 */
	public static function create( array $data = [], $secret, $algorithm = self::HS512, $notBeforeOffset = 10, $expireOffset = 60, $serverName = 'localhost'){
		
		$tokenId = base64_encode ( md5(Util::create_GUID()) );
		$issuedAt = time ();
		$notBefore = $issuedAt + $notBeforeOffset; // Adding 10 seconds
		$expire = $notBefore + $expireOffset; // Adding 60 seconds
		
		$body = [
				'iat' => $issuedAt, // Issued at: time when the token was generated
				'jti' => $tokenId, // Json Token Id: an unique identifier for the token
				'iss' => $serverName, // Issuer
				'nbf' => $notBefore, // Not before
				'exp' => $expire, // Expire
				'data' =>$data //The data to send as array
		];
		
		$secretKey = base64_decode ( $secret );
		
		/*
		 * Encode the array to a JWT string.
		 * 
		 * The output string can be validated at http://jwt.io/
		 */
		$jwt = JWT::encode( $body, base64_decode ( $secret ), $algorithm);
		
		return $jwt;
	}
}