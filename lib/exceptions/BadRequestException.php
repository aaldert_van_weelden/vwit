<?php 
/**
 * Bad Request exception class. Thrown when client issues a bad HTTP request method
 * 
 * @author Aaldert van Weelden
 */
class BadRequestException extends Exception{
	
	const TAG = "BAD REQUEST EXCEPTION: ";
	protected $message;
	protected $code;
	
	public function __construct($message='Bad request method', $code=0){
		$this->message=$message;
		if($code==0){
			$this->code=ExceptionCode::BADREQUEST;
		}else{
		    $this->code=$code;
		}
		
		
		parent::__construct(self::TAG.$this->message, $this->code);
	}
	
	
	
	protected function setMessage($message){
		$this->message=$message;
	}
	
	protected function setCode($code){
		$this->code=$code;
	}
	
    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}


?>