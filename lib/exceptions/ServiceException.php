<?php 
/**
 * Service layer exception class
 * 
 * @author Aaldert van Weelden
 */
class ServiceException extends Exception{
	
	const TAG = "SERVICE EXCEPTION: ";
	protected $message;
	protected $code;
	
	public function __construct($message='Service layer fault', $code=0){
		$this->message=$message;
		if($code==0){
			$this->code=ExceptionCode::SERVICE_ERROR;
		}else{
		    $this->code=$code;
		}
		
		
		parent::__construct(self::TAG.$this->message, $this->code);
	}
	
	
	
	protected function setMessage($message){
		$this->message=$message;
	}
	
	protected function setCode($code){
		$this->code=$code;
	}
	
    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}


?>