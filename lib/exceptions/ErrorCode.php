<?php
use VWIT\Base\Enum;
/**
 * @see Element
 *
 * @author Aaldert van Weelden
 *
 */
//TODO transfer this to elements for clientside exposure
class ErrorCode extends Enum{
	
	const UNKNOWN 						= 0;
	const USER_LOGINFAIL 				= 1;
	const USER_EXISTS 					= 2;
	const USER_INVALID_CREDENTIALS 		= 3;
	const USER_ALREADY_AUTHENTICATED 	= 4;
	const PAYMENT_NO_ISSUERS			= 501;
	const PAYMENT_DIRECTORY_REQUEST		= 502;
	const PAYMENT_TRANSACTION_REQUEST	= 520;
	const PAYMENT_STATUS_REQUEST		= 540;
	const PAYMENT_REFUND_REQUEST		= 560;
	const PAYMENT_ON_NOTIFICATION		= 580;
	
	//default value
	public $value = self::UNKNOWN;
	
	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($value = null){
		return new ErrorCode($value);
	}
	
}





?>