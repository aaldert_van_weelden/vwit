<?php
use VWIT\Base\Enum;
/**
 * Class used to define error codes
 *
 * @author Aaldert van Weelden
 */
class ExceptionCode extends Enum {
	const XHR_ERROR = 40;
	const AUTHENTICATION_ERROR = 50;
	const BASIC_AUTH = 55;
	const DATABASE_ERROR = 100;
	const PERSISTENCE = 150;
	const OBJECT_NOT_FOUND = 170;
	const LOCKED_ENTRY = 180;
	const SERVICE_ERROR = 190;
	const INVALID_TYPE = 200;
	const INVALID_STRUCTURE = 210;
	const KEY_INVALID = 220;
	const STATUS_INVALID = 230;
	const INVALID_ARGUMENT = 240;
	const INVALID_ACTION = 245;
	const PROPERTY = 250;
	const PAYMENT = 260;
	const INVALID_COMPONENT = 300;
	const MISSING_PARAMETER = 310;
	const NULL_ARGUMENT = 400;
	const NOT_UNIQUE = 450;
	const ACCESS_ERROR = 500;
	const BADREQUEST = 501;
	const IMPORTER = 600;
	const XML_PARSE_EXCEPTION = 610;
	const JWT_DECODE = 620;
	const JWT_NULLPOINTER = 630;
	const INDEXABLE = 700;
	const SOAPFAULT = 800;
	const PARSE_ERROR = 900;
	public $value = null;
	
	/*
	 * (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($value = null) {
		return new ExceptionCode ( $value );
	}
}

?>