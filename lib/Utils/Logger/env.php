<?php 
/**
 * Include this file if usage of the project  .env file is not possible
 */


  $_ENV['log-default-tag'] = 'VWIT_LOGGER';

/*
 * enable query logging from Eloquent listener, see app/start/local.php
 */

  $_ENV['log-query'] = true;

  
  
 /*
  * logging levels to write to debug.log
  * allowed values are TRACE, VERBOSE,DEBUG,INFO,NOTICE,WARN,ERROR
  */
  
  $_ENV['log-level'] = 'TRACE';

  
  
 /*
  * logging filtering, add inclusion or exclusion TAG comma-separated format, case insensitive
  */
  
  $_ENV['log-exclude'] = 'ServiceProvider,App::before, DomainRouter,AuthenticationService,ElementHelper,JsonHelper,TranslationHelper';
 
  //filtering the logging message and tag,  comma-separated format, case insensitive
  $_ENV['log-include'] = '';
  
  //replace or append in the log->out dump file
  $_ENV['log-out-append'] = true;

  
  
  $_ENV['remote_adress'] = '127.0.0.1';
  
  $_ENV['log_dir'] = 'C:/Users/Aaldert/phplogs/';
  
  $_ENV['DEFAULT_LOGFILE_NAME'] = 'elw-info-debug.log';
  
  $_ENV['ERROR_LOGFILE_NAME'] = 'elw-error-warn.log';
  
  $_ENV['QUERY_LOGFILE_NAME'] = 'elw-query.log'

?>