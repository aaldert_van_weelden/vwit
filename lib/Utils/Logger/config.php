<?php
/** \file config.php
* Main configuration script.
* For development configuration see config_local.inc.php
*
* @author Aaldert van Weelden
* @date 23-07-2012
**/
if(isset($_ENV['log-level']) ) {
	//logging levels to write to debug.log
	if (!defined('LOGLEVEL')) define('LOGLEVEL',$_ENV['log-level']);
	
	if (!defined('LOG_QUERY')) define('LOG_QUERY',$_ENV['log-query']);
	
	if (!defined('LOG_DEFAULT_TAG')) define('LOG_DEFAULT_TAG', isset($_ENV['log-default-tag'])?$_ENV['log-default-tag']:'APP_LOGGER');
	
	//logging exclusion filter
	if( isset($_ENV['log-exclude']) ){
		if (!defined('LOGFILTER')) define('LOGFILTER',$_ENV['log-exclude']);
	}else{
		if (!defined('LOGFILTER')) define('LOGFILTER',null);
	}
	
	//logging inclusion filter
	if( isset($_ENV['log-include']) ){
		if (!defined('LOG_INCLUDE')) define('LOG_INCLUDE',$_ENV['log-include']);
	}else{
		if (!defined('LOG_INCLUDE')) define('LOG_INCLUDE',null);
	}
	
	//replace or append in the log->out dump file
	if( isset($_ENV['log-out-append']) ){
		if (!defined('LOG_OUT_APPEND')) define('LOG_OUT_APPEND',$_ENV['log-out-append']);
	}else{
		if (!defined('LOG_OUT_APPEND')) define('LOG_OUT_APPEND',null);
	}
	
	/**
	 * Environment specific configuration data
	 *
	 */
	if( isset($_SERVER['REMOTE_ADDR']) ){
		if (!defined('REMOTE_ADRESS')) define("REMOTE_ADRESS",$_SERVER['REMOTE_ADDR']);
	}else{
		if (!defined('REMOTE_ADRESS')) define("REMOTE_ADRESS",$_ENV['remote_adress']);
	}
	
	//log directory and logfiles
	if (!defined('LOG_DIR')) define("LOG_DIR", $_ENV['log_dir']);
	if (!defined('DEFAULT_LOGFILE_NAME')) define("DEFAULT_LOGFILE_NAME", $_ENV['DEFAULT_LOGFILE_NAME']);// The name of the log file for levels DEBUG and INFO
	if (!defined('ERROR_LOGFILE_NAME')) define("ERROR_LOGFILE_NAME", $_ENV['ERROR_LOGFILE_NAME']);// The name of the log file for levels WARN and ERROR
	
	if (!defined('QUERY_LOGFILE_NAME')) define("QUERY_LOGFILE_NAME", $_ENV['QUERY_LOGFILE_NAME']);// The name of the log file for the mySQL Queries
} else {
	
	if (!defined('LOGLEVEL')) define('LOGLEVEL',env('log-level'));
	
	if (!defined('LOG_QUERY')) define('LOG_QUERY',env('log-query'));
	
	if (!defined('LOG_DEFAULT_TAG')) define('LOG_DEFAULT_TAG', env('log-default-tag')?env('log-default-tag'):'APP_LOGGER');
	
	//logging exclusion filter
	if( env('log-exclude') ){
		if (!defined('LOGFILTER')) define('LOGFILTER',env('log-exclude'));
	}else{
		if (!defined('LOGFILTER')) define('LOGFILTER',null);
	}
	
	//logging inclusion filter
	if( env('log-include') ){
		if (!defined('LOG_INCLUDE')) define('LOG_INCLUDE',env('log-include'));
	}else{
		if (!defined('LOG_INCLUDE')) define('LOG_INCLUDE',null);
	}
	
	//replace or append in the log->out dump file
	if( env('log-out-append') ){
		if (!defined('LOG_OUT_APPEND')) define('LOG_OUT_APPEND',env('log-out-append'));
	}else{
		if (!defined('LOG_OUT_APPEND')) define('LOG_OUT_APPEND',null);
	}
	
	/**
	 * Environment specific configuration data
	 *
	 */
	if( isset($_SERVER['REMOTE_ADDR']) ){
		if (!defined('REMOTE_ADRESS')) define("REMOTE_ADRESS",$_SERVER['REMOTE_ADDR']);
	}else{
		if (!defined('REMOTE_ADRESS')) define("REMOTE_ADRESS",env('remote_adress'));
	}
	
	//log directory and logfiles
	if (!defined('LOG_DIR')) define("LOG_DIR", env('log_dir'));
	if (!defined('DEFAULT_LOGFILE_NAME')) define("DEFAULT_LOGFILE_NAME", env('DEFAULT_LOGFILE_NAME'));// The name of the log file for levels DEBUG and INFO
	if (!defined('ERROR_LOGFILE_NAME')) define("ERROR_LOGFILE_NAME", env('ERROR_LOGFILE_NAME'));// The name of the log file for levels WARN and ERROR
	
	if (!defined('QUERY_LOGFILE_NAME')) define("QUERY_LOGFILE_NAME", env('QUERY_LOGFILE_NAME'));// The name of the log file for the mySQL Queries
}

?>
