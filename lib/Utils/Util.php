<?php
namespace Utils;
use Utils\StringConstant as StringConstant;
/**
 * global utitliy helper functions
 *
 * @author Aaldert van Weelden
 */
class Util
{

	const TAG = 'Util';
	private static $instance;
	private static $dumpCount=0;
	private static $instanceCount=0;

	private function __construct(){

	}

	public static function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new self();
		}
		self::$instanceCount++;
		return self::$instance;
	}

	/**
	 * Maak een unique sleutelwaarde.
	 */
	public static function create_GUID($seed = 0) {
	    $microTime = microtime();
	    list($a_dec, $a_sec) = explode(" ", $microTime);

	    $seed += crc32(md5($microTime));
	    mt_srand($seed);

	    $dec_hex = sprintf("%x", $a_dec* 1000000 + $seed);
	    $sec_hex = sprintf("%x", $a_sec + $seed);

	    self::ensure_length($dec_hex, 5);
	    self::ensure_length($sec_hex, 6);

	    $guid = "";
	    $guid .= $dec_hex;
	    $guid .= self::create_guid_section(3);
	    $guid .= '-';
	    $guid .= self::create_guid_section(4);
	    $guid .= '-';
	    $guid .= self::create_guid_section(4);
	    $guid .= '-';
	    $guid .= self::create_guid_section(4);
	    $guid .= '-';
	    $guid .= $sec_hex;
	    $guid .= self::create_guid_section(6);
	    return $guid;
	}

	public static function create_guid_section($characters) {
		$return = "";
		for($i=0; $i<$characters; $i++) {
			$return .= sprintf("%x", mt_rand(0,15));
		}
		return $return;
	}

	public static function ensure_length(&$string, $length) {
		$strlen = strlen($string);
		if($strlen < $length) {
			$string = str_pad($string,$length,"0");
		}
		else if($strlen > $length){
			$string = substr($string, 0, $length);
		}
	}

	/**
	 * Create a custum password
	 *
	 */
	public static function create_password() {
		$length    = 7;
		$key_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$rand_max  = strlen($key_chars) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand_pos  = rand(0, $rand_max);
            $rand_key[] = $key_chars[$rand_pos];
		}
		$rand_pass = implode('', $rand_key);
		return ($rand_pass);
	}


	/**
	 * Validates a valid v4 UUID string
	 * @param string $id
	 * @return boolean
	 */
	public static function isUUID($id){
		$UUID = '/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i';
		if( preg_match($UUID, $id)){
			return true;
		}
		return false;
	}
	/**
	 * Validates a valid v4 UUID string
	 * @param string $id
	 * @return boolean
	 */
	public static function isUUID4($id){
		$UUIDv4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
		if( preg_match($UUIDv4, $id)){
			return true;
		}
		return false;
	}


	/**
	 * Generate random string
	 * @param integer $length
	 * @return string
	 */
	public static function random_string($length) {
		return substr(hash('sha256', mt_rand()), 0, $length);
	}


	/**
	 * Create a random numberstring
	 *
	 */
	public static function create_number($length = 7) {

		$key_chars = '123456789123456789123456789123456789';
		$rand_max  = strlen($key_chars) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand_pos  = rand(0, $rand_max);
			$rand_key[] = $key_chars[$rand_pos];
		}
		$rand_pass = implode('', $rand_key);
		return ($rand_pass);
	}



	//JSON
	public static function json_ready($data){

		$replace=array('\\t','\\n','\\',' ');
		$data=str_replace($replace,'',$data);
		$data=str_replace('"{','{',$data);
		$data=str_replace('}"','}',$data);
		return $data;

	}

	public static function json2object($data){
		$result = array();
		$data=ltrim($data,"[");
		$data=rtrim($data,"]");
		$aData=explode("},",$data);

		if(is_array($aData) && count($aData)>0){
			foreach($aData as $element){
				$obj = array();
				$element=str_replace('{','',$element);
				$element=str_replace('"','',$element);
	 			$aElement = explode(",",$element);

	 			if(is_array($aElement) && count($aElement)>0){
					foreach($aElement as $row){
						$aProp=explode(":",$row);
						if(is_array($aProp) && count($aProp)==2){
							$key = trim($aProp[0]);
							$value= trim($aProp[1]);
							$obj[$key] = $value;
						}
					}
	 			}

				$result[]= (object) $obj;
			}
		}
		return $result;
	}




    /*
    * USE WITH JAVASCRIPT UTIL FUNCTION CLIENTSIDE
	* safeURI 		:
    * @description encodes uri-component to base64 decoded JSON-object for sending as url
    * @method safeURI
    * @public
    * @static
    * @param {string}  uri
    * @return {string} decoded object
	*/

	/**
	 * javascript methods:
	 *
	 * safeURI : function(uri,mode){
            typeof(mode)==='undefined'||mode==='' ||!mode ? mode='false':void(0);
			uri=this.URLtoObj(uri);//make object
			uri=_JSON.stringify(uri);//object to string
            switch(mode){
                case 'base64':
                    uri=_base64.encode(uri);//base64encode
                    break;
                case 'base64safe':
                    uri=_base64.encode(uri,true);//base64encode
                    break;
            }
			return uri;
		},

		safeObjToURI : function(obj,mode){
			typeof(mode)==='undefined'||mode==='' ||!mode ? mode='base64':void(0);
			var uri=_JSON.stringify(obj);//object to string
			switch(mode){
	            case 'base64':
	                uri=_base64.encode(uri);//base64encode
	                break;
	            case 'base64safe':
	                uri=_base64.encode(uri,true);//base64encode
	                break;
	        }
			return uri;
		},
	 *
	 */


    public static function uriToJSONstring($data,$sEncode=false){
            switch($sEncode){
                case 'base64':
                    $data=base64_decode($data);
                    $data=utf8_encode($data);
                    break;
                case 'base64safe':
                    $data=base64_decode($data);
                    $data=Util::charcodeToTxt($data);
                    $data=utf8_encode($data);
                    break;
                default :
                    //
                    break;

            }
            $data=urldecode($data);

            return $data;

    }

    public static function uriToObject($data,$sEncode=false){
    	switch($sEncode){
    		case 'base64':
    			$data=base64_decode($data);
    			$data=utf8_encode($data);
    			break;
    		case 'base64safe':
    			$data=base64_decode($data);
    			$data=Util::charcodeToTxt($data);
    			$data=utf8_encode($data);
    			break;
    		default :
    			//
    			break;

    	}
    	$data=urldecode($data);

    	return json_decode($data);

    }

    public static function cleanupForJSON($data){
    	$data = str_replace (array("\r\n", "\n", "\r", "\t"), '', $data);
    	$data = str_replace (array("\""), '\"', $data);

    	return $data;
    }

	public static function charcodeToTxt($value){
		$result='';

		$arr = explode('&', $value);
		foreach($arr as $key=>$val){

			$sub=explode(';', $val);
			isset($sub[0])?$one=$sub[0]:$one='';
			isset($sub[1])?$two=$sub[1]:$two='';

			if( strstr($one,'#') && strlen($one)>1 ){
				$code=intval( str_replace('#','',$sub[0]) );
				if($code>0){
					//var_dump($code)
					$one=chr($code);
				}
			}
			$result.=$one.$two;
		}
		return $result;
	}



	public static function getServerHash($key){
		return md5( utf8_encode($key) );
	}

	public static function libxml_display_error($error) {
		$return = "\n";
		switch ($error->level) {
			case LIBXML_ERR_WARNING:
				$return .= "Warning: $error->code: ";
				break;
			case LIBXML_ERR_ERROR:
				$return .= "Error: $error->code: ";
				break;
			case LIBXML_ERR_FATAL:
				$return .= "Fatal Error: $error->code: ";
				break;
		}
		$return .= trim($error->message);
		if ($error->file) {
			$return .= " in $error->file";
		}
		$return .= " on line $error->line\n";

		return $return;
	}

	public static function libxml_display_errors() {
		$errors = libxml_get_errors();
		$err='';
		foreach ($errors as $error) {
			$err.=self::libxml_display_error($error);
		}
		libxml_clear_errors();
		return $err;

	}

	/**
	 * Validate an object property
	 *
	 * @param object $in
	 * @param string $property  Can be NULL, in that case the first parameter is evaluated
	 * @param boolean $zeroAllowed,
	 *        	default FALSE
	 * @return boolean TRUE if the property exists and is not null or empty
	 */
	private static function isNotNullOrEmpty($in, $property = null, $zeroAllowed = false) {
		if ($property === null) {

			if (is_object ( $in ) && count ( (array) $in ) === 0 )
				return false;
			$isNotEmpty = $zeroAllowed ? ! empty ( $in ) : $in === 0 || $in === '0' || ! empty ( $in );
			if (is_string ( $in )) {
				return $isNotEmpty && strtoupper ( $in ) !== 'NULL' && strtoupper ( $in ) !== 'FALSE' && strtoupper ( $in ) !== 'UNDEFINED';
			} else {
				return $isNotEmpty;
			}
		}

		if (! is_object ( $in ))
			throw new \Exception ( 'Provided test $in must be a class instance(object) or a valid classname(string)' );
		if (! property_exists ( $in, $property ))
			return false;
		$prop = $in->{$property};
		$isNotEmpty = $zeroAllowed ? ! empty ( $prop ) : $prop === 0 || $prop === '0' || ! empty ( $prop );
		if (is_string ( $prop )) {
			return $isNotEmpty && strtoupper ( $prop ) !== 'NULL' && strtoupper ( $prop ) !== 'FALSE' && strtoupper ( $prop ) !== 'UNDEFINED';
		} else {
			return $isNotEmpty;
		}
	}

	/**
	 * Validate an object property
	 *
	 * @param object $in
	 * @param string $property  Can be NULL, in that case the first parameter is evaluated
	 * @return boolean TRUE if the property exists and is not (NULL or  empty)
	 */
	public static function notNullOrEmpty($in, $property = null) {
		return self::isNotNullOrEmpty ( $in, $property );
	}

	/**
	 * Validate an object property
	 *
	 * @param object $in
	 * @param string $property  Can be NULL, in that case the first parameter is evaluated
	 * @return boolean TRUE if the property exists and is not (NULL or empty or zero)
	 */
	public static function notNullOrEmptyOrZero($in, $property = null) {
		return self::isNotNullOrEmpty ( $in, $property, true );
	}

	/**
	 * Validate an object property
	 *
	 * @param object $in
	 * @param string $property  Can be NULL, in that case the first parameter is evaluated
	 * @return boolean TRUE if the property exists and is NULL or empty
	 */
	public static function nullOrEmpty($in, $property = null) {
		return ! self::isNotNullOrEmpty ( $in, $property );
	}

	/**
	 * Validate an object property
	 *
	 * @param object $in
	 * @param string $property  Can be NULL, in that case the first parameter is evaluated
	 * @return boolean TRUE if the property exists and is NULL or empty or zero
	 */
	public static function nullOrEmptyOrZero($in, $property = null) {
		return ! self::isNotNullOrEmpty ( $in, $property, true );
	}




	public static function parseNullOrEmpty($in=null){

		return self::nullOrEmpty($in)?null:$in;
	}

	/**
	 * Return TRUE if the input isNotNull AND input is valid array AND input size > 0
	 * @param array $in
	 * @return boolean
	 */
	public static function isValidArray($in=array()){
		return (self::notNullOrEmpty($in) && is_array($in) && count($in)>0);
	}

	public static function br2nl($s, $useReturn=false) {
	    return preg_replace('/(<br ?\/?>)/i', (($useReturn) ? '\r' : '\n'), $s);
	}

	public static function noBreak($s) {
		return preg_replace('/(<br ?\/?>)/i', '', $s);
	}

	/**
	 * return the array of underscore versions of the camelcase propertynames
	 * @param Array $properties
	 */
	public static function getFieldNames($properties){
		$props = array();
		foreach($properties as $prop){
			$name = self::camelCaseToUnderScore($prop);
			$props[$name]=$prop;
		}
		return $props;
	}

	/**
	 * return the underscore version of a camelcase name : CamelCase => camel_case
	 * @param String $name
	 */
    public static function camelCaseToUnderScore($name) {

		return strtolower(preg_replace(
				array('/\\\\/', '/(?<=[a-z])([A-Z])/', '/__/'),
				array('_', '_$1', '_'),
				ltrim($name, '\\')
		));
	}


	public static function arrayToOBject(/*array*/ $input){
	    return json_decode(json_encode($input));
	}

	public static function RemoveDir($dir, $DeleteMe) {

		if(!$dh = @opendir($dir)) return;

		while (false !== ($obj = readdir($dh))) {

			if($obj=='.' || $obj=='..') continue;
			if (!@unlink($dir.'/'.$obj)) {RemoveDir($dir.'/'.$obj, true);}
		}

		closedir($dh);

		if ($DeleteMe){
			@rmdir($dir);// verwijder dir
		}
	}

	/*
	 *
	 * array(6) {
		  ["file"]=>
		  string(89) "C:\Users\Gebruiker\git\orm\orm\orm_project\lib\model\test\subject.dto.conversion_test.php"
		  ["line"]=>
		  int(77)
		  ["function"]=>
		  string(6) "create"
		  ["class"]=>
		  string(6) "Entity"
		  ["type"]=>
		  string(17) "static scope [::]"
		  ["args"]=>
		  array(0) {
		  }
		}
	 */
	public static function getCaller($html=true){
		if(LOGLEVEL=='VERBOSE' || LOGLEVEL=='DEBUG'){
			$e = new \Exception();
			$trace = $e->getTrace();
			//position 0 would be the line that called this function so we ignore it
			$last_call = $trace[1];
			$type = null;
			$sep = null;
			switch($last_call['type']){
				case '->':
					$type="object scope [->]";
					$sep = '->';
					break;
				case '::':
					$type='static scope [::]';
					$sep = '::';
					break;

			}
			$last_call['type']=$type;

			$out = "\n<b>Methode ".$last_call['class'].$sep.$last_call['function']."() is called from :</b>\n";


			$out.= "\t--> file     : ".$last_call['file'].", line : ".$last_call['line']."\n";
			$out.= "\t--> function : ".$last_call['function']."\n";
			$out.= "\t--> class    : ".$last_call['class']."\n";
			$out.= "\t--> type     : ".$last_call['type']."\n";
			$out.= "\t--> args     : ".implode(",",$last_call['args'])."\n";

			if(!$html){
			   return $out;
			}
			print '<div style="font-family:Courier New, Courier, monospace;font-size:12px;">';
			print nl2br($out);
			print '</div>';
			return;
		}

	}

	/**
	 * Helper to dump the content to screen, can be expanded and collapsed
	 * @param object $object
	 * @param string $caption
	 * @param boolean $halt
	 */
	public static function dump($object, $caption=null, $halt=false){
		self::$dumpCount++;
		$ID = self::$dumpCount.'-'.self::$instanceCount;
		print '<br>';
		print '<span style="display:visible;cursor:pointer;" id="plus_'.$ID.'" onclick="
			document.getElementById(\'plus_'.$ID.'\').style.display=\'none\';
			document.getElementById(\'minus_'.$ID.'\').style.display=\'inline\';
			document.getElementById(\'dump_'.$ID.'\').style.display=\'block\';
		">[+]&nbsp;</span>';
		print '<span style="display:none;cursor:pointer;" id="minus_'.$ID.'" onclick="
			document.getElementById(\'plus_'.$ID.'\').style.display=\'inline\';
			document.getElementById(\'minus_'.$ID.'\').style.display=\'none\';
			document.getElementById(\'dump_'.$ID.'\').style.display=\'none\';
		">[-]&nbsp;</span>';
		if($caption!=null){
			print '<b>'.$caption.' : </b><br>';
		}
		print '<span style="display:none;" class="util_dump" id="dump_'.$ID.'">';
		print StringConstant::PR;
		var_dump($object);
		print StringConstant::PE;
		print '</span>';
		if($halt){
			exit();
		}
	}

	/**
	 * Helper to dump the content to screen, not collapsed
	 * @param object $object
	 * @param string $caption
	 * @param boolean $halt
	 */
	public static function dumpAndShow($object, $caption=null, $halt=false){
		self::$dumpCount++;
		$ID = self::$dumpCount.'-'.self::$instanceCount;
		print '<br>';
		if($caption!=null){
			print '<b>'.$caption.' : </b><br>';
		}
		print StringConstant::PR;
		var_dump($object);
		print StringConstant::PE;
		if($halt){
			exit();
		}
	}


	public static function out($in,$var){
		$hr = "\n\n---------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
		var_dump($hr."*** $in :",$var,$hr);
	}

	/**
	 *    Identity test. Drops back to equality + types for PHP5
	 *    objects as the === operator counts as the
	 *    stronger reference constraint.
	 *    @param mixed $first    Test subject.
	 *    @param mixed $second   Comparison object.
	 *    @return boolean        True if identical.
	 *    @access public
	 */
	public static function isEqual($first, $second) {
		if (version_compare(phpversion(), '5') >= 0) {
			return self::isIdenticalType($first, $second);
		}
		if ($first != $second) {
			return false;
		}
		return ($first === $second);
	}


	/**
	 *    Recursive type test.
	 *    @param mixed $first    Test subject.
	 *    @param mixed $second   Comparison object.
	 *    @return boolean        True if same type.
	 *    @access private
	 */
	private static function isIdenticalType($first, $second) {
		if (gettype($first) != gettype($second)) {
			return false;
		}
		if (is_object($first) && is_object($second)) {
			if (get_class($first) != get_class($second)) {
				return false;
			}
			return self::isArrayOfIdenticalTypes(
					(array) $first,
					(array) $second);
		}
		if (is_array($first) && is_array($second)) {
			return self::isArrayOfIdenticalTypes($first, $second);
		}
		if ($first !== $second) {
			return false;
		}
		return true;
	}



	/**
	 *    Recursive type test for each element of an array.
	 *    @param mixed $first    Test subject.
	 *    @param mixed $second   Comparison object.
	 *    @return boolean        True if identical.
	 *    @access private
	 */
	private static function isArrayOfIdenticalTypes($first, $second) {
		if (array_keys($first) != array_keys($second)) {
			return false;
		}
		foreach (array_keys($first) as $key) {
			$is_identical = self::isIdenticalType(
					$first[$key],
					$second[$key]);
			if (! $is_identical) {
				return false;
			}
		}
		return true;
	}

	public static function hasEqualPropertyValues($obj1,$obj2,$strict=false){
		if(is_object($obj1)){
			if(is_object($obj2)){
				$obj1Properties = array();
				foreach($obj1 as $key=>$value){
					$obj1Properties[$key]=$value;
				}

				$obj2Properties = array();
				foreach($obj2 as $key=>$value){
					$obj2Properties[$key]=(array)$value;
					if(is_object($value)){
						if(!self::hasEqualPropertyValues($obj1Properties[$key],$value)){
							return false;
						}
					}else{
						if(!isset($obj1Properties[$key])){
						    trigger_error(self::TAG.': no property exists for key <'.$key.'> to compare with',E_USER_WARNING);
							if($strict){
								return false;
							}else{
								break;
							}
						}
						if($value != $obj1Properties[$key]){

							trigger_error(self::TAG.': values do not match for key <'.$key.'> => '
									.print_r($value,true).' , original value = '
									.print_r($obj1Properties[$key],true),E_USER_WARNING);
							return false;
						}
					}
				}
				return true;
			}else{
				return false;
			}
		}else{
			if(is_object($obj2)){
				return false;
			}
			return $obj1===obj2;
		}

	}



	public static function strip_word_html($text, $allowed_tags = '<b><i><sup><sub><em><strong><u><br>')
	{
		mb_regex_encoding('UTF-8');
		//replace MS special characters first
		$search = array('/&lsquo;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u');
		$replace = array('\'', '\'', '"', '"', '-');
		$text = trim($text);
		$text = preg_replace($search, $replace, $text);
		//make sure _all_ html entities are converted to the plain ascii equivalents - it appears
		//in some MS headers, some html entities are encoded and some aren't
		$text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
		//try to strip out any C style comments first, since these, embedded in html comments, seem to
		//prevent strip_tags from removing html comments (MS Word introduced combination)
		if(mb_stripos($text, '/*') !== FALSE){
			$text = mb_eregi_replace('#/\*.*?\*/#s', '', $text, 'm');
		}
		//introduce a space into any arithmetic expressions that could be caught by strip_tags so that they won't be
		//'<1' becomes '< 1'(note: somewhat application specific)
		$text = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
		$text = strip_tags($text, $allowed_tags);
		//eliminate extraneous whitespace from start and end of line, or anywhere there are two or more spaces, convert it to one
		$text = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $text);
		//strip out inline css and simplify style tags
		$search = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
		$replace = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
		$text = preg_replace($search, $replace, $text);
		//on some of the ?newer MS Word exports, where you get conditionals of the form 'if gte mso 9', etc., it appears
		//that whatever is in one of the html comments prevents strip_tags from eradicating the html comment that contains
		//some MS Style Definitions - this last bit gets rid of any leftover comments */
		$num_matches = preg_match_all("/\<!--/u", $text, $matches);
		if($num_matches){
			$text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
		}
		return trim($text);
	}

}