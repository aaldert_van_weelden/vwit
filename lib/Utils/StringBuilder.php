<?php namespace Utils;
/**
 * helperclass to provide structured string building tools
 */
class StringBuilder{

	const PRINT_STR = true;

	protected $result;


	public function __construct($str=null){
		$this->result = [];
		if($str!=null){
			$this->result[] = (string) $str;
		}
	}

	/**
	 * Append the input to the result string
	 * @param mixed $in
	 */
	public function append($in=StringConstant::EMP){

		$this->result[] = (string) $in;
	}

	/**
	 * Append the input to the result string with an EOL added
	 * @param mixed $in
	 */
	public function appendEOL($in=StringConstant::EMP){

		$this->append($in.StringConstant::EOL);
	}

	/**
	 * Prepend the input to the result string
	 * @param mixed $in
	 */
	public function prepend($in=StringConstant::EMP){

		array_unshift($this->result, (string) $in);
	}

	/**
	 * Prepend the input to the result string with an EOL added
	 * @param mixed $in
	 */
	public function prependEOL($in=StringConstant::EMP){

		array_unshift($this->result, (string) $in.StringConstant::EOL);
	}

	/**
	 * Append the oject dump to the string
	 * @param object $obj
	 */
	public function appendObject($obj=null){

		$in = StringConstant::HR;
		$in.= print_r($obj,true);
		$in.= StringConstant::HR;
		$this->append($in);
	}

	/**
	 * @deprecated Use nl2br or __toString <br>
	 *
	 * @param boolean $prnt If StringBuilder::PRINT , then print the string
	 * @param boolean $nl2br If TRUE, the output is converted: \n -> &lt;br&gt;
	 * @return string output
	 */
	public function toString($prnt=false, $nl2br = false){
		$out = (string) implode('',$this->result);
		if($nl2br===true){
			$out = nl2br($out);
		}
		if($prnt===true){
			print $out;
		}else{
			return $out;
		}

	}

	/**
	 * Return or output the  converted  \n -> &lt;br&gt; string
	 * @param boolean $prnt If StringBuilder::PRINT, then print the HTML string
	 * @return string HTML output
	 */
	public function nl2br($prnt=false){
		$out = (string) implode('',$this->result);
		$out = nl2br($out);

		if($prnt!==self::PRINT_STR){
			return $out;
		}
		print $out;
	}

	/**
	 * Determine if there exists a valid entry in the resultset
	 * @return boolean
	 */
	public function isEmpty(){

		if(!is_array($this->result)){
			$this->result = [];
			return true;
		}

		foreach ($this->result as $entry){
			if(empty($entry)) continue;
			return false;
		}
		return true;
	}

	/**
	 *
	 * @return string
	 */
	public function __toString(){
		return (string) implode('',$this->result);
	}

	public function clear(){
		$this->result = [];
	}

	/**
	 * Return the raw resultset
	 */
	public function raw(){
		return $this->result;
	}

}
?>