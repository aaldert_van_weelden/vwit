<?php namespace Utils;
/**
 * class used to adress batch MySQL functions
 * @author Aaldert van Weelden
 *
 */

class MySQL_util{

	/**
	 * store the sqldump of the database as backup in the configured location
	 */
	public static function backup(){
		trigger_error('backing up database '.DATABASE_NAME, E_USER_NOTICE);


		$command = 'mysqldump -B --user='.DATABASE_USER.' --password='.DATABASE_PASS.' --host='.DATABASE_HOST.' '.DATABASE_NAME.' | gzip -9 > '.SQL_BACKUP;
		trigger_error('Executing command: '.$command,E_USER_NOTICE);
		exec($command);
	}
}

?>