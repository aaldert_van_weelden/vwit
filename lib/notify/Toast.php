<?php namespace VWIT;

/*
 * Include in main stylesheet or add to show() method as HTML
 *
 * Styles:
 .toast {
 display: block;
 min-heigth: 50px;
 border: 1px solid #e0e0e0;
 padding:5px;
 clear:both;
 text-align: center;
 }

 .toast-title {
 font-weight: bold;
 font-size: 1.1em;
 margin-bottom:5px;
 }

 .toast-message {
 font-weight: normal;
 font-size: 1.0em;
 }

 .toast-success {
 color: #155724;
 background-color: #d4edda;
 border-color: #c3e6cb;
 }

 .toast-info {
 color: #464a4e;
 background-color: #e7e8ea;
 border-color: #dddfe2;
 }

 .toast-warning {
 color: #856404;
 background-color: #fff3cd;
 border-color: #ffeeba;
 }

 .toast-error {
 color: #721c24;
 background-color: #f8d7da;
 border-color: #f5c6cb;
 }
 */

/**
 * Class used to show persistent user messages with a configurable (sticky) time-to-live<br>
 *
 * <u>Examples:</u> <br>
 * Toast::set()->key($user);//optional, default is tbb_user<br>
 * Toast::set()->ttl(5);//optional, default is 2 seconds<br>
 *
 * <u>The available message types:</u>
 * Toast::set()->error('Show this user error', 'The error title',10); //custom TTL of 10 seconds<br>
 * Toast::set()->info('Show this user info', 'The info title', 5); //custom TTL of 5 seconds<br>
 * Toast::set()->warning('Show this user warning', 'The warning title');<br>
 * Toast::set()->success('Show this user success', 'The success title');<br>
 *
 * Toast::set()->success('Show this user success'); //no title specified<br>
 *
 * <u>Show the HTML messages in the view.</u> Add this in the main view, eg header.php<br>
 * Toast::show();<br>
 *
 * @author aaldert.vanweelden
 *
 */
class Toast {

	const SUCCESS = 'SUCCESS';
	const INFO = 'INFO';
	const WARNING = 'WARNING';
	const ERROR = 'ERROR';

	const SESS_KEY = 'toastkey';
	const SESS_REGISTRY = 'toastregistry';

	const STICKY = true;
	const NON_STICKY = false;

	private static $instance = null;
	private static $counter = 0;

	public $key;

	private $ttl = 2;

	private function __construct() {

		if (session_status() !== PHP_SESSION_ACTIVE) session_start();

		if (key_exists(self::SESS_KEY, $_SESSION)) {
			$this->key = $_SESSION[self::SESS_KEY];
		} else {
			$this->key = $_SESSION[self::SESS_KEY] = 'toast2_' . $_SESSION['tbb_user'];
		}

		if (! key_exists(self::SESS_REGISTRY, $_SESSION)) {
			$_SESSION[self::SESS_REGISTRY] = [];
		}
	}

	/**
	 * Return the singleton instance
	 *
	 * @return Toast
	 */
	public static function set() {

		if (self::$instance === null) {
			self::$instance = new Toast();
		}
		return self::$instance;
	}

	/**
	 * Set the session key, default is toast2_{tbb_user}
	 *
	 * @param string $val
	 */
	public function key($val) {

		$this->key = $val;
	}

	/**
	 * Set the TTL, default is 2 seconds
	 *
	 * @param integer $seconds
	 */
	public function ttl($seconds) {

		$this->ttl = $seconds;
	}

	/**
	 *
	 * @param string $message
	 *        	The success message
	 * @param string $title
	 *        	Optional title, printed bold
	 * @param boolean $sticky
	 * 			Optional sticky flag, default NON_STICKY
	 * @param integer $ttl
	 *        	Optional message TTL, default the singleton value is used
	 * @see self::$ttl
	 */
	public function success($message, $title = null, $sticky = self::NON_STICKY, $ttl = null) {

		if ($ttl === null) $ttl = $this->ttl;
		$this->push((object) [
			'time' => time(),
			'severity' => self::SUCCESS,
			'ttl' => $ttl,
			'message' => $message,
			'title' => $title,
			'sticky' =>  $sticky,
			'sender' => key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI']: null
		]);
	}

	/**
	 *
	 * @param string $message
	 *        	The info message
	 * @param string $title
	 *        	Optional title, printed bold
	 * @param boolean $sticky
	 * 			Optional sticky flag, default NON_STICKY
	 * @param integer $ttl
	 *        	Optional message TTL, default the singleton value is used
	 * @see self::$ttl
	 */
	public function info($message, $title = null, $sticky = self::NON_STICKY, $ttl = null) {
		if ($ttl === null) $ttl = $this->ttl;
		$this->push((object) [
			'time' => time(),
			'severity' => self::INFO,
			'ttl' => $ttl,
			'message' => $message,
			'title' => $title,
			'sticky' =>  $sticky,
			'sender' => key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI']: null
		]);
	}

	/**
	 *
	 * @param string $message
	 *        	The warning message
	 * @param string $title
	 *        	Optional title, printed bold
	 * @param boolean $sticky
	 * 			Optional sticky flag, default NON_STICKY
	 * @param integer $ttl
	 *        	Optional message TTL, default the singleton value is used
	 * @see self::$ttl
	 */
	public function warning($message, $title = null, $sticky = self::NON_STICKY, $ttl = null) {

		if ($ttl === null) $ttl = $this->ttl;
		$this->push((object) [
			'time' => time(),
			'severity' => self::WARNING,
			'ttl' => $ttl,
			'message' => $message,
			'title' => $title,
			'sticky' =>  $sticky,
			'sender' => key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI']: null
		]);
	}

	/**
	 *
	 * @param string $message
	 *        	The error message
	 * @param string $title
	 *        	Optional title, printed bold
	 * @param boolean $sticky
	 * 			Optional sticky flag, default NON_STICKY
	 * @param integer $ttl
	 *        	Optional message TTL, default the singleton value is used
	 * @see self::$ttl
	 */
	public function error($message, $title = null, $sticky = self::NON_STICKY, $ttl = null) {

		if ($ttl === null) $ttl = $this->ttl;
		$this->push((object) [
			'time' => time(),
			'severity' => self::ERROR,
			'ttl' => $ttl,
			'message' => $message,
			'title' => $title,
			'sticky' =>  $sticky,
			'sender' => key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI']: null
		]);
	}

	/**
	 * Validate the message and cleanup the cache
	 *
	 * @param object $body
	 */
	private function push($body) {

		$found = false;
		if (! empty($_SESSION[self::SESS_REGISTRY][$this->key]) && is_array($_SESSION[self::SESS_REGISTRY][$this->key])) {
			foreach ($_SESSION[self::SESS_REGISTRY][$this->key] as $key => $cached) {

				if ($body->message == $cached->message && $body->severity == $cached->severity && $body->title == $cached->title) {
					$found = true;
				}
				if (time() > $cached->time + $chached->ttl) {
					unset($_SESSION[self::SESS_REGISTRY][$this->key][$key]);
					$found = false;
				}
			}
		}
		if (! $found) {
			$_SESSION[self::SESS_REGISTRY][$this->key][$body->time . '_' . self::$counter] = $body;
			self::$counter ++;
		}
	}

	/**
	 * Return HTML
	 *
	 * @return string
	 */
	public static function show($print = true) {

		$self = self::set();
		$html = '';
		$sender = key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI']: null;

		if (! empty($_SESSION[self::SESS_REGISTRY][$self->key]) && is_array($_SESSION[self::SESS_REGISTRY][$self->key])) {
			foreach ($_SESSION[self::SESS_REGISTRY][$self->key] as $key => $body) {

				if (time() > $body->time + $body->ttl) {
					unset($_SESSION[self::SESS_REGISTRY][$self->key][$key]);
				} else {
					if( $body->sender === $sender || $body->sticky) {
						$html .= '<div class="toast toast-'.strtolower($body->severity).'">';
						if(! empty($body->title)) $html .= '<span class="toast-title">' . $body->title . '</span><br>';
						$html .= '<span class="toast-message">' . $body->message . '</span><br>';
						$html .= '</div>';
					}
				}
			}
			if ($print) print $html;
		}
		return $html;
	}
}