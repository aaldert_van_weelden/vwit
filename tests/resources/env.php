<?php 
/**
 * Include this file if usage of the project  .env file is not possible
 */

/*
 * Default logging tag / sender, overwrite by stting the $tag variable
 */

$_ENV['log-default-tag'] = 'VWIT_LOGGER';

/*
 * enable query logging from Eloquent listener, see app/start/local.php
 */

  $_ENV['log-query'] = true;

  
  
 /*
  * logging levels to write to debug.log
  * allowed values are TRACE, VERBOSE,DEBUG,INFO,NOTICE,WARN,ERROR
  */
  
  $_ENV['log-level'] = 'TRACE';

  
  
 /*
  * logging filtering, add inclusion or exclusion TAG comma-separated format, case insensitive
  */
  
  $_ENV['log-exclude'] = '';
 
  //filtering the logging message and tag,  comma-separated format, case insensitive
  $_ENV['log-include'] = '';

  
  
  $_ENV['remote_adress'] = '127.0.0.1';
  
  $_ENV['log_dir'] = ROOT.'/tests/out/';
  
  $_ENV['DEFAULT_LOGFILE_NAME'] = 'vwit-info-debug.log';
  
  $_ENV['ERROR_LOGFILE_NAME'] = 'vwit-error-warn.log';
  
  $_ENV['QUERY_LOGFILE_NAME'] = 'vwit-query.log'

?>