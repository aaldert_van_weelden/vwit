<?php
if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
require_once ROOT.'/tests/TestCase.php';

use VWIT\Base\ENUM_PARAM;


/**
 * @runTestsInSeparateProcesses
 */
class EnumTranslationTest extends TestCase {

	private $error;
	
	public function setup(){
		//catching user errors from Enum for assertion
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$this->error =  "\n".$errstr . " on line " . $errline . " in file " . $errfile."\n";
		});
	}

	public function teardown(){
		restore_error_handler();
	}
	
	/**
	 * Instantiate enum type without default
	 * Param = null
	 * Param = default
	 * Param = type value
	 * Param = invalid key
	 */
	public function testNoTranslationsNoDefault(){
	
		$type = TypeNoDefault::get(Type::THIRD, ENUM_PARAM::NO_DEFAULT_VALUE);
		
		$this->assertEquals(Type::THIRD, $type->i18nValue());
		//no translations, so the key is returned
		$this->assertEquals(Type::FIRST, $type->i18nValue(Type::FIRST));
		$this->assertEquals(Type::SECOND, $type->i18nValue(Type::SECOND));
		$this->assertEquals(Type::THIRD, $type->i18nValue(Type::THIRD));
		$this->assertEquals(Type::FOURTH, $type->i18nValue(Type::FOURTH));
		
	}
	
	public function testNoTranslationsNoDefaultIllegalKey(){
	
		$type = TypeNoDefault::get(Type::SECOND);
	
		$this->assertEquals(Type::SECOND, $type->i18nValue());

		$this->assertNotEquals('foo', $type->i18nValue('foo'));
		$this->assertContains('Invalid key [foo] detected', $this->error);
	
		$this->assertEquals('?[foo]?', $type->i18nValue('foo'));
		$this->assertContains('Invalid key [foo] detected', $this->error);
	
	}
	
	/**
	 * Instantiate enum type without default
	 * Param = null
	 * Param = default
	 * Param = type value
	 * Param = invalid key
	 */
	public function testTypeTranslationsNoDefault(){
	
		$type = TypeNoDefault::get(Type::THIRD);
		
		$type->setI18n('third_value_translation');
	
		$this->assertEquals('third_value_translation', $type->i18nValue());
		$this->assertEquals(Type::FIRST, $type->i18nValue(Type::FIRST));
		$this->assertEquals(Type::SECOND, $type->i18nValue(Type::SECOND));
		$this->assertEquals('third_value_translation', $type->i18nValue(Type::THIRD));
		$this->assertEquals(Type::FOURTH, $type->i18nValue(Type::FOURTH));
	
	}
	
	
	/**
	 * Instantiate enum type without default
	 * Param = null
	 * Param = default
	 * Param = type value
	 * Param = invalid key
	 */
	public function testArrayTranslationsNoDefault(){
	
		$type = TypeNoDefault::get(Type::THIRD);
	
		$translations = [
			null => 'default_translation',
			Type::FIRST => 'first_translation',
			Type::SECOND => 'second_translation',
			Type::THIRD => 'third_translation',
			Type::FOURTH => 'fourth_translation',
		];
		
		$type->setI18n($translations);
		
		$this->assertContains('Unnecessary translation entries detected', $this->error);
	
		$this->assertEquals($translations[Type::THIRD], $type->i18nValue());
		$this->assertEquals($translations[Type::FIRST], $type->i18nValue(Type::FIRST));
		$this->assertEquals($translations[Type::SECOND], $type->i18nValue(Type::SECOND));
		$this->assertEquals($translations[Type::THIRD], $type->i18nValue(Type::THIRD));
		$this->assertEquals($translations[Type::FOURTH], $type->i18nValue(Type::FOURTH));
	
	}
	
	
	/**
	 * Instantiate enum type without default
	 * Param = null
	 * Param = default
	 * Param = type value
	 * Param = invalid key
	 */
	public function testNoTranslationsWithDefault(){
	
		$type = Type::get(Type::THIRD);
	
		$this->assertEquals(Type::THIRD, $type->i18nValue());
		$this->assertEquals(Type::FIRST, $type->i18nValue(Type::FIRST));
		$this->assertEquals(Type::SECOND, $type->i18nValue(Type::SECOND));
		$this->assertEquals(Type::THIRD, $type->i18nValue(Type::THIRD));
		$this->assertEquals(Type::FOURTH, $type->i18nValue(Type::FOURTH));
	
	}
	
	public function testNullTypeNoTranslationsWithDefault(){
	
		$type = Type::get(null);
	
		$this->assertEquals(null, $type->i18nValue());
		$this->assertEquals(Type::FIRST, $type->i18nValue(Type::FIRST));
		$this->assertEquals(Type::SECOND, $type->i18nValue(Type::SECOND));
		$this->assertEquals(Type::THIRD, $type->i18nValue(Type::THIRD));
		$this->assertEquals(Type::FOURTH, $type->i18nValue(Type::FOURTH));
	
	}
	
	/**
	 * Instantiate enum type with default
	 * Param = null
	 * Param = default
	 * Param = type value
	 * Param = invalid key
	 */
	public function testTypeTranslationsWithDefault(){
	
		$type = Type::get(Type::THIRD);
	
		$type->setI18n('third_value_translation');
	
		$this->assertEquals('third_value_translation', $type->i18nValue());
		$this->assertEquals(Type::FIRST, $type->i18nValue(Type::FIRST));
		$this->assertEquals(Type::SECOND, $type->i18nValue(Type::SECOND));
		$this->assertEquals('third_value_translation', $type->i18nValue(Type::THIRD));
		$this->assertEquals(Type::FOURTH, $type->i18nValue(Type::FOURTH));
	
	}
	
	/**
	 * 
	 * Instantiate enum type with default
	 * Param = null
	 * Param = default
	 * Param = type value
	 * Param = invalid key
	 */
	public function testNullTypeTranslationsWithDefault(){
	
		$type = Type::get();
	
		$type->setI18n('default_value_translation');
		
		$this->assertEquals('default_value_translation', $type->i18nValue());
		$this->assertEquals(Type::FIRST, $type->i18nValue(Type::FIRST));
		$this->assertEquals(Type::SECOND, $type->i18nValue(Type::SECOND));
		$this->assertEquals(Type::THIRD, $type->i18nValue(Type::THIRD));
		$this->assertEquals(Type::FOURTH, $type->i18nValue(Type::FOURTH));
	
	}
	
	
	/**
	 * Instantiate enum type without default
	 * Param = null
	 * Param = default
	 * Param = type value
	 * Param = invalid key
	 */
	public function testArrayTranslationsWithDefault(){
	
		$type = Type::get(Type::THIRD);
	
		$translations = [
				null => 'default_translation',
				Type::FIRST => 'first_translation',
				Type::SECOND => 'second_translation',
				Type::THIRD => 'third_translation',
				Type::FOURTH => 'fourth_translation',
		];
	
		$type->setI18n($translations);
		
		//var_dump($type);
	
		$this->assertEquals($translations[Type::THIRD], $type->i18nValue());
		$this->assertEquals($translations[Type::FIRST], $type->i18nValue(Type::FIRST));
		$this->assertEquals($translations[Type::SECOND], $type->i18nValue(Type::SECOND));
		$this->assertEquals($translations[Type::THIRD], $type->i18nValue(Type::THIRD));
		$this->assertEquals($translations[Type::FOURTH], $type->i18nValue(Type::FOURTH));
	
	}
	
	
	/**
	 * Instantiate enum type without default
	 * Param = null
	 * Param = default
	 * Param = type value
	 * Param = invalid key
	 */
	public function testNulltypeArrayTranslationsWithDefault(){
	
		$type = Type::get();
	
		$translations = [
				null => 'default_translation',
				Type::FIRST => 'first_translation',
				Type::SECOND => 'second_translation',
				Type::THIRD => 'third_translation',
				Type::FOURTH => 'fourth_translation',
		];
	
		$type->setI18n($translations);
	
		//var_dump($type);
	
		$this->assertEquals($translations[null], $type->i18nValue());
		$this->assertEquals($translations[Type::FIRST], $type->i18nValue(Type::FIRST));
		$this->assertEquals($translations[Type::SECOND], $type->i18nValue(Type::SECOND));
		$this->assertEquals($translations[Type::THIRD], $type->i18nValue(Type::THIRD));
		$this->assertEquals($translations[Type::FOURTH], $type->i18nValue(Type::FOURTH));
	
	}
	
	
	
	
}