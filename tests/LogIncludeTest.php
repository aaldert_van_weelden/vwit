<?php
use Utils\Logger\Logger;
use Utils\Util;

if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
if(!defined('ALLOWED')) define ('ALLOWED', '192.168.1.5');

require_once ROOT.'/tests/TestCase.php';


/**
 * @runTestsInSeparateProcesses
 */
class LogIncludeTest extends TestCase {

	
	public function setup(){
		
	}
	
	public function teardown(){
		Util::RemoveDir(LOG_DIR, false);
	}
	
	
	public function testSetup(){
		
		require_once ROOT.'/tests/resources/env.include.php';
		require_once ROOT.'/tests/resources/config.php';
		
		Util::RemoveDir(LOG_DIR, false);
		
		$this->assertTrue(defined('LOGLEVEL'));
		$this->assertTrue(defined('LOG_QUERY'));
		$this->assertTrue(defined('LOGFILTER'));
		$this->assertTrue(defined('LOG_INCLUDE'));
		$this->assertTrue(defined('REMOTE_ADRESS'));
		$this->assertTrue(defined('LOG_DIR'));
		$this->assertTrue(defined('DEFAULT_LOGFILE_NAME'));
		$this->assertTrue(defined('ERROR_LOGFILE_NAME'));
		$this->assertTrue(defined('QUERY_LOGFILE_NAME'));
		
		$this->assertEquals(LOGLEVEL, $_ENV['log-level']);
		$this->assertEquals(LOG_QUERY, $_ENV['log-query']);
		$this->assertEquals(LOGFILTER, $_ENV['log-exclude']);
		$this->assertEquals(LOG_INCLUDE, $_ENV['log-include']);
		$this->assertEquals(REMOTE_ADRESS, $_ENV['remote_adress']);
		$this->assertEquals(LOG_DIR, $_ENV['log_dir']);
		$this->assertEquals(DEFAULT_LOGFILE_NAME, $_ENV['DEFAULT_LOGFILE_NAME']);
		$this->assertEquals(ERROR_LOGFILE_NAME, $_ENV['ERROR_LOGFILE_NAME']);
		$this->assertEquals(QUERY_LOGFILE_NAME, $_ENV['QUERY_LOGFILE_NAME']);
		
	}
	
	
	
	/**
	 * Test whitelist
	 */
	public function testIncludeIP(){
		
		require_once ROOT.'/tests/resources/env.include.php';
		$_ENV['remote_adress'] = '192.168.1.5';
		
		require_once ROOT.'/tests/resources/config.php';
		
		Util::RemoveDir(LOG_DIR, false);
		
		$this->assertContains( $_ENV['remote_adress'], LOG_INCLUDE);
		
		$this->assertFalse(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		$this->assertFalse(file_exists ( LOG_DIR.ERROR_LOGFILE_NAME ));
		$this->assertFalse(file_exists ( LOG_DIR.QUERY_LOGFILE_NAME ));
	
		
		Logger::info('test');
		$this->assertTrue(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		
		Logger::error("test");
		$this->assertTrue(file_exists ( LOG_DIR.ERROR_LOGFILE_NAME ));
		
		Logger::query("test");
		$this->assertTrue(file_exists ( LOG_DIR.QUERY_LOGFILE_NAME ));
		
	}
	
	
	/**
	 * Test the IP exclusion
	 *
	 * @return void
	 */
	public function testNotIncludeIP(){
		
		require_once ROOT.'/tests/resources/env.include.php';
		$_ENV['remote_adress'] = '10.50.10.1';
		
		require_once ROOT.'/tests/resources/config.php';
		
		Util::RemoveDir(LOG_DIR, false);
		
		$this->assertNotContains('10.50.10.1', LOG_INCLUDE);
	
		$this->assertFalse(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		$this->assertFalse(file_exists ( LOG_DIR.ERROR_LOGFILE_NAME ));
		$this->assertFalse(file_exists ( LOG_DIR.QUERY_LOGFILE_NAME ));
	
		Logger::info('test');
		$this->assertFalse(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		
		Logger::error("test");
		$this->assertTrue(file_exists ( LOG_DIR.ERROR_LOGFILE_NAME ));
	
		Logger::query("test");
		$this->assertFalse(file_exists ( LOG_DIR.QUERY_LOGFILE_NAME ));
	
	}

}
