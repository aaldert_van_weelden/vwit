<?php
if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
require_once ROOT.'/tests/TestCase.php';

use VWIT\Base\Enum;







/**
 * @runTestsInSeparateProcesses
 */
class EnumReviveTest extends TestCase {

	
private $error;
	
	public function setup(){
		//catching user errors from Enum for assertion
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$this->error =  "\n".$errstr . " on line " . $errline . " in file " . $errfile."\n";
		});
	}

	public function teardown(){
		restore_error_handler();
	}
	
	public function testProperties(){
		$type = TypeNoDefault::get(Type::THIRD);
	
		$this->assertEquals(Type::THIRD, $type->value());
		$this->assertEquals('THIRD', $type->name());
		$this->assertEquals(2, $type->ordinal());	
		
		$type = Type::get(Type::THIRD);
		
		$this->assertEquals(Type::THIRD, $type->value());
		$this->assertEquals('THIRD', $type->name());
		$this->assertEquals(2, $type->ordinal());
	}
	
	public function testreviveByObject(){
		$type = TypeDefaultSet::get(TypeDefaultSet::THIRD);
		
		$obj = (object) [
				'clazz' => 'TypeDefaultSet',
				'value' => 'third_value',
		];
	
		$revived = Enum::revive($obj);
		
		$this->assertEquals($type, $revived);
		
	}
	
	public function testreviveByArgument(){
		$type = TypeDefaultSet::get(TypeDefaultSet::THIRD);
	
		$obj = (object) [
				'value' => 'third_value',
		];
	
		$revived = Enum::revive($obj, 'TypeDefaultSet');
	
		$this->assertEquals($type, $revived);
	
	}
	
}


?>