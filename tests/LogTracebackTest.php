<?php
use Utils\Logger\Logger;
use Utils\Util;
use Utils\Logger\LoggerInstance;

if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));

require_once ROOT.'/tests/TestCase.php';

/**
 * @runTestsInSeparateProcesses
 */
class LogTracebackTest extends TestCase {
	
	const TAG = 'TestTAG';
	const MESSAGE = 'TestMessage';
	private $log;
	
	public function setup(){
		require_once ROOT.'/tests/resources/include.inc';
		
		Util::RemoveDir(LOG_DIR, false);
	}
	
	public function teardown(){
		Util::RemoveDir(LOG_DIR, false);
	}
	
	
	public function testSetup(){
		//everything cleaned up?
		$this->assertFalse(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		$this->assertFalse(file_exists ( LOG_DIR.ERROR_LOGFILE_NAME ));
		$this->assertFalse(file_exists ( LOG_DIR.QUERY_LOGFILE_NAME ));
		
		//check the config
		$this->assertTrue(defined('LOGLEVEL'));
		$this->assertTrue(defined('LOG_QUERY'));
		$this->assertTrue(defined('LOGFILTER'));
		$this->assertTrue(defined('LOG_INCLUDE'));
		$this->assertTrue(defined('REMOTE_ADRESS'));
		$this->assertTrue(defined('LOG_DIR'));
		$this->assertTrue(defined('DEFAULT_LOGFILE_NAME'));
		$this->assertTrue(defined('ERROR_LOGFILE_NAME'));
		$this->assertTrue(defined('QUERY_LOGFILE_NAME'));
		
		$this->assertEquals(LOGLEVEL, $_ENV['log-level']);
		$this->assertEquals(LOG_QUERY, $_ENV['log-query']);
		$this->assertEquals(LOGFILTER, $_ENV['log-exclude']);
		$this->assertEquals(LOG_INCLUDE, $_ENV['log-include']);
		$this->assertEquals(REMOTE_ADRESS, $_ENV['remote_adress']);
		$this->assertEquals(LOG_DIR, $_ENV['log_dir']);
		$this->assertEquals(DEFAULT_LOGFILE_NAME, $_ENV['DEFAULT_LOGFILE_NAME']);
		$this->assertEquals(ERROR_LOGFILE_NAME, $_ENV['ERROR_LOGFILE_NAME']);
		$this->assertEquals(QUERY_LOGFILE_NAME, $_ENV['QUERY_LOGFILE_NAME']);
		
		//print "\n".'REMOTE_ADDRESS: '.REMOTE_ADRESS."\n";
		//print LOG_DIR.DEFAULT_LOGFILE_NAME."\n";	
	}
	
	/**
	 * Test the logging
	 *
	 * @return void
	 */
	public function testLogging(){
		
		Logger::debug("test");
		$this->assertTrue(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
	
		Logger::error("test");
		$this->assertTrue(file_exists ( LOG_DIR.ERROR_LOGFILE_NAME ));
	
		Logger::query("test");
		$this->assertTrue(file_exists ( LOG_DIR.QUERY_LOGFILE_NAME ));
	
	}
	
	public function testStaticLogger(){
		
		Logger::log()->debug(self::MESSAGE);
		
		$this->assertTrue(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		$line = $this->getDebugLog();
		$this->assertContains(self::MESSAGE, $this->getDebugLog());
		$this->assertNotContains(self::TAG, $this->getDebugLog());
		$this->assertContains(LOG_DEFAULT_TAG, $this->getDebugLog());
		$this->assertContains( __CLASS__, $this->getDebugLog());
	}
	
	public function testClassLogger(){
		
		$this->log = new LoggerInstance(null);
		
		$this->log->debug(self::MESSAGE);
		
		$this->assertTrue(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		$this->assertContains(self::MESSAGE, $this->getDebugLog());
		$this->assertNotContains(self::TAG, $this->getDebugLog());
		$this->assertContains(LOG_DEFAULT_TAG, $this->getDebugLog());
		$this->assertContains( __CLASS__, $this->getDebugLog());
	}
	
	public function testLoggerInstanceWithTag(){
		
		Logger::log(self::TAG)->debug(self::MESSAGE);
		
		$this->assertTrue(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		$this->assertContains(self::MESSAGE, $this->getDebugLog());
		$this->assertContains(self::TAG, $this->getDebugLog());
		$this->assertNotContains(LOG_DEFAULT_TAG, $this->getDebugLog());
		$this->assertContains(__CLASS__, $this->getDebugLog());
	}
	
	public function testClassLoggerWithTag(){
		$this->log = new LoggerInstance(self::TAG);
		
		$this->log->debug(self::MESSAGE);
		
		$this->assertTrue(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		$this->assertContains(self::MESSAGE, $this->getDebugLog());
		$this->assertContains(self::TAG, $this->getDebugLog());
		$this->assertNotContains(LOG_DEFAULT_TAG, $this->getDebugLog());
		$this->assertContains(__CLASS__, $this->getDebugLog());
	}
	
	
	private function getDebugLog(){
		return file_get_contents(LOG_DIR.DEFAULT_LOGFILE_NAME);
	}
	
	

}
