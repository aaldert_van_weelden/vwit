<?php
use Utils\Logger\Logger;
use Utils\Util;

if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));

require_once ROOT.'/tests/TestCase.php';

/**
 * @runTestsInSeparateProcesses
 */
class LoggerTest extends TestCase {

	
	public function setup(){
		require_once ROOT.'/tests/resources/include.inc';
		
		Util::RemoveDir(LOG_DIR, false);
	}
	
	public function teardown(){
		Util::RemoveDir(LOG_DIR, false);
	}
	
	public function testSetup(){
		$this->assertTrue(defined('LOGLEVEL'));
		$this->assertTrue(defined('LOG_QUERY'));
		$this->assertTrue(defined('LOGFILTER'));
		$this->assertTrue(defined('LOG_INCLUDE'));
		$this->assertTrue(defined('REMOTE_ADRESS'));
		$this->assertTrue(defined('LOG_DIR'));
		$this->assertTrue(defined('DEFAULT_LOGFILE_NAME'));
		$this->assertTrue(defined('ERROR_LOGFILE_NAME'));
		$this->assertTrue(defined('QUERY_LOGFILE_NAME'));
		
		$this->assertEquals(LOGLEVEL, $_ENV['log-level']);
		$this->assertEquals(LOG_QUERY, $_ENV['log-query']);
		$this->assertEquals(LOGFILTER, $_ENV['log-exclude']);
		$this->assertEquals(LOG_INCLUDE, $_ENV['log-include']);
		$this->assertEquals(REMOTE_ADRESS, $_ENV['remote_adress']);
		$this->assertEquals(LOG_DIR, $_ENV['log_dir']);
		$this->assertEquals(DEFAULT_LOGFILE_NAME, $_ENV['DEFAULT_LOGFILE_NAME']);
		$this->assertEquals(ERROR_LOGFILE_NAME, $_ENV['ERROR_LOGFILE_NAME']);
		$this->assertEquals(QUERY_LOGFILE_NAME, $_ENV['QUERY_LOGFILE_NAME']);
		
	}
	
	/**
	 * Test the logging
	 *
	 * @return void
	 */
	public function testLoggingToFile(){
		//print "\n".'REMOTE_ADDRESS: '.REMOTE_ADRESS."\n";
		//print LOG_DIR.DEFAULT_LOGFILE_NAME."\n";
		$this->assertFalse(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
		$this->assertFalse(file_exists ( LOG_DIR.ERROR_LOGFILE_NAME ));
		$this->assertFalse(file_exists ( LOG_DIR.QUERY_LOGFILE_NAME ));
	
		Logger::debug("test");
		$this->assertTrue(file_exists ( LOG_DIR.DEFAULT_LOGFILE_NAME ));
	
		Logger::error("test");
		$this->assertTrue(file_exists ( LOG_DIR.ERROR_LOGFILE_NAME ));
	
		Logger::query("test");
		$this->assertTrue(file_exists ( LOG_DIR.QUERY_LOGFILE_NAME ));
		
	}

}
