<?php
if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
require_once ROOT.'/tests/TestCase.php';



/**
 * @runTestsInSeparateProcesses
 */
class EnumTest extends TestCase {

	
private $error;
	
	public function setup(){
		//catching user errors from Enum for assertion
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$this->error =  "\n".$errstr . " on line " . $errline . " in file " . $errfile."\n";
		});
	}

	public function teardown(){
		restore_error_handler();
	}
	
	public function testProperties(){
		$type = TypeNoDefault::get(Type::THIRD);
	
		$this->assertEquals(Type::THIRD, $type->value());
		$this->assertEquals('THIRD', $type->name());
		$this->assertEquals(2, $type->ordinal());	
		
		$type = Type::get(Type::THIRD);
		
		$this->assertEquals(Type::THIRD, $type->value());
		$this->assertEquals('THIRD', $type->name());
		$this->assertEquals(2, $type->ordinal());
		
		//default value
		$type = Type::get(null);
		
		$this->assertNull($type->value());
		$this->assertEquals('__default', $type->name());
		$this->assertNull($type->ordinal());
		
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testNullvalueWithNoDefault() {
		$type = TypeNoDefault::get ( null );
	}
	
	/**
	 * 
	 */
	public function testInvalidTranslationkey() {
		$type = Type::get ();
		$key = 'invalid';
		$string = $type->i18nValue($key);
		$this->assertContains('ENUM: Invalid key ['.$key.'] detected', $this->error);
	}
	
	
}


/*



out('* switch example:');
$test = 'second_value';

switch($test){
    case Type::FIRST:
        out(0);
        break;
    case Type::SECOND:
        out(1, 'Detected SECOND');
        break;
    case Type::THIRD:
        out(2);
        break;
}




out('* view example:');

$vars = ['type'=>$type];

out($vars['type']->i18nValue, 'translation');
out($vars['type']->name(), 'name');
out($vars['type']->value(),'value');
out($vars['type']->ordinal(), 'ordinal');


*/

?>