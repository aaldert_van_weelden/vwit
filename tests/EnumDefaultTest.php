<?php
if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
require_once ROOT.'/tests/TestCase.php';

use VWIT\Base\Enum;


/**
 * @runTestsInSeparateProcesses
 */
class EnumDefaultTest extends TestCase {

	
private $error;
	
	public function setup(){
		//catching user errors from Enum for assertion
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$this->error =  "\n".$errstr . " on line " . $errline . " in file " . $errfile."\n";
		});
	}

	public function teardown(){
		restore_error_handler();
	}
	
	public function testProperties(){
		$type = TypeNoDefault::get(Type::THIRD);
		
		$this->assertEquals(Type::THIRD, $type->value());
		$this->assertEquals('THIRD', $type->name());
		$this->assertEquals(2, $type->ordinal());	
		
		$type = Type::get(Type::THIRD);
		
		$this->assertEquals(Type::THIRD, $type->value());
		$this->assertEquals('THIRD', $type->name());
		$this->assertEquals(2, $type->ordinal());
		
		//default value
		$type = Type::get(null);
		
		$this->assertNull($type->value());
		$this->assertEquals('__default', $type->name());
		$this->assertNull($type->ordinal());
		
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testNullvalueWithNoDefault() {
		$type = TypeNoDefault::get ( null );
	}
	
	public function testDefaultSet(){
		$type = TypeDefaultSet::get();
		
		$this->assertEquals(TypeDefaultSet::FIRST, $type->value());
		$this->assertEquals('FIRST', $type->name());
		$this->assertEquals(0, $type->ordinal());
	}
	
	
	public function testInvalidTranslationkey() {
		$type = Type::get ();
		$key = 'invalid';
		$string = $type->i18nValue($key);
		$this->assertContains('ENUM: Invalid key ['.$key.'] detected', $this->error);
	}
	
	public function testValueSetting(){
		
		$user_role = 6;
		$this->assertTrue( Role::get($user_role)->hasLessPermissionsThen(Role::SYSTEM) );
		
		$user_role = 2;
		$this->assertFalse( Role::get($user_role)->hasLessPermissionsThen(Role::SYSTEM) );
		
		$user_role = 3;
		$this->assertTrue( Role::get($user_role)->hasLessPermissionsThen(Role::SYSTEM) );
		
	}
	
	
}
?>