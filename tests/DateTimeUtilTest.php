<?php
use Utils\Util;
use Utils\DateTimeUtil;

if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));

require_once ROOT.'/tests/TestCase.php';

/**
 * @runTestsInSeparateProcesses
 */
class DateTimeUtilTest extends TestCase {

	
	public function setup(){
		
		
	}
	
	public function teardown(){
		
	}
	
	
	public function testSetup(){
		
	}
	
	public function testDiff(){
		
		$older = '';
		$newer = '';
		
		$diff = DateTimeUtil::diff($older, $newer);
		
		$this->assertEquals($diff, 0);
		
		$older = '2017-04-08 00:00:00';
		$newer = '2017-04-10 00:00:00';
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::SECOND);
		$this->assertEquals($diff, 172800);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::MINUTE);
		$this->assertEquals($diff, 2880);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::HOUR);
		$this->assertEquals($diff, 48);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::DAY);
		$this->assertEquals($diff, 2);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::WEEK);
		$this->assertEquals($diff, 0);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::MONTH);
		$this->assertEquals($diff, 0);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::YEAR);
		$this->assertEquals($diff, 0);
		
		
		
		$older = '2017-03-08 00:00:00';
		$newer = '2017-04-10 00:00:00';
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::SECOND);
		$this->assertEquals($diff, 2851200);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::MINUTE);
		$this->assertEquals($diff, 47520);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::HOUR);
		$this->assertEquals($diff, 792);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::DAY);
		$this->assertEquals($diff, 33);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::WEEK);
		$this->assertEquals($diff, 5);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::MONTH);
		$this->assertEquals($diff, 1);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::YEAR);
		$this->assertEquals($diff, 0);
		
	}
	
	public function testRoundingDiff(){
		
		
		$older = '2017-03-08 00:00:00';
		$newer = '2017-04-10 11:59:00';
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::DAY);
		$this->assertEquals($diff, 33);
		
		$older = '2017-03-08 00:00:00';
		$newer = '2017-04-10 12:00:00';
		
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::DAY);
		$this->assertEquals($diff, 34);
	}
	
	public function testAllDiff(){
	
	
		$older = '2016-03-08 00:00:00';
		$newer = '2017-04-10 11:59:44';
	
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::SECOND);
		$this->assertEquals($diff, 34430384);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::MINUTE);
		$this->assertEquals($diff, 573840);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::HOUR);
		$this->assertEquals($diff, 9564);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::DAY);
		$this->assertEquals($diff, 398);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::WEEK);
		$this->assertEquals($diff,  57);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::MONTH);
		$this->assertEquals($diff, 13);
		
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::YEAR);
		$this->assertEquals($diff, 1);
	
		
	}
	
	
	public function testBeforeAfterDiff(){
	
	
		$older = '2017-03-08 00:00:00';
		$newer = '2017-04-10 11:59:44';
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::SECOND);
		$this->assertEquals($diff, 2894384);
		
		$older = '2017-03-08 00:00:00';
		$newer = '2017-03-08 00:00:00';
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::SECOND);
		$this->assertEquals($diff, 0);
		
		$older = '2017-03-08 00:00:00';
		$newer = '2017-03-08 00:00:01';
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::SECOND);
		$this->assertEquals($diff, 1);
	
		$older = '2017-03-08 00:00:01';
		$newer = '2017-03-08 00:00:00';
		$diff = DateTimeUtil::diff($older, $newer, DateTimeUtil::SECOND);
		$this->assertEquals($diff, -1);
	
	
	}
	
	
	public function testAddToTimestamp(){
		
		$creation = '2017-03-08 00:00:00';
		$ttl = 3600;
		
		$expire = DateTimeUtil::addToTimeStamp($creation, (integer) $ttl/3600);
		
		$this->assertEquals($expire, '2017-03-08 01:00:00');
	}
	
	
	

}
