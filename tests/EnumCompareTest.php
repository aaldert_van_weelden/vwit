<?php

use VWIT\Base\Enum;
use VWIT\Base\ENUM_CMP;
use VWIT\Base\ENUM_PARAM;

class Type02 extends Enum{

	const FIRST = 'FIRST_VALUE';
	const SECOND = 'A_SECOND_VALUE';
	const THIRD = 'THIRD_VALUE';

	public $default = self::FIRST;

	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($setValue = null){
	    return new Type02($setValue, ENUM_PARAM::ADD_DEFAULT_VALUE);
	}
}

class Test extends Enum{

	const FIRST = 'FIRST_VALUE';
	const SECOND = 'A_SECOND_VALUE';
	const THIRD = 'THIRD_VALUE';

	public $default = self::FIRST;

	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($setValue = null){
	    return new Test($setValue, ENUM_PARAM::ADD_DEFAULT_VALUE);
	}
}

//-----------------------------------------
//type to compare to

/*

$type = Type::get(Type::SECOND);

dump($type->constants(), 'reference key-values pairs');

$test = Test::get();

out('* checking the enums for equality');

dump($type->equals($test), 'Non-strict equality check  (default) , only the KV-pairs');

dump($type->equals($test, true), 'Strict (type-aware) equality check by object');

out(Type::SECOND,'Compare with ');

//-------------------------------------------

out('* Test::__default = null');

$test->setValue(null);

show();

//---------------------------------------------------

out('* Test::FIRST = '.Test::FIRST);

$test->setValue(Test::FIRST);

show();
//---------------------------------------------------

out('* Test::SECOND = '.Test::SECOND);

$test->setValue(Test::SECOND);

show();
//---------------------------------------------------

out('* Test::THIRD = '.Test::THIRD);

$test->setValue(Test::THIRD);

show();
//---------------------------------------------------

function show(){
    global $type;
    global $test;

    dump($type->compare($test),'Comparing by ordinals (default)');

    dump($type->compare($test,ENUM_CMP::INT),'Comparing the enum values as int');

    dump($type->compare($test, ENUM_CMP::STR),'Comparing the enum string values');

    dump($type->equals($test->value), 'Non-strict equality check  (default) by value');

    dump($type->equals($test->value, true), 'Strict (type-aware) equality check by value');

}

*/

?>