<?php
use Utils\StringBuilder;
use Utils\StringConstant;

if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
require_once ROOT.'/tests/TestCase.php';


/**
 * @runTestsInSeparateProcesses
 */
class StringBuilderTest extends TestCase {

	
private $error;
	
	public function setup(){
		//catching user errors from Enum for assertion
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$this->error =  "\n".$errstr . " on line " . $errline . " in file " . $errfile."\n";
		});
	}

	public function teardown(){
		restore_error_handler();
	}
	
	public function test__toString(){
	
		$s = new StringBuilder();
		$s->append('test');
		$this->assertNotNull($s->__toString());
		$this->assertFalse(empty($s->__toString()));
		$this->assertEquals('test', $s->__toString());
	}
	
	public function testConstruct(){
		
		$s = new StringBuilder();
		$p = new StringBuilder('test');
		
		$this->assertEquals('', $s->__toString());
		$this->assertEquals('test', $p->__toString());
	}
	
	/**
	 * Append the input to the result string
	 * @param mixed $in
	 */
	public function testAppend(){
		
		$s = new StringBuilder();
		$s->append();
		$this->assertEquals('', $s->__toString());
		$s->append('one');
		$this->assertEquals('one', $s->__toString());
		$s->append('two');
		$this->assertEquals('onetwo', $s->__toString());
		$s->append('three');
		$this->assertEquals('onetwothree', $s->__toString());
		
	}
	
	/**
	 * Append the input to the result string with an EOL added
	 * @param mixed $in
	 */
	public function testAppendEOL(){
		$s = new StringBuilder();
		$s->appendEOL();
		$this->assertEquals("\r\n", $s->__toString());
		$s->appendEOL('one');
		$this->assertEquals("\r\none\r\n", $s->__toString());
		$s->appendEOL('two');
		$this->assertEquals("\r\none\r\ntwo\r\n", $s->__toString());
		$s->appendEOL('three');
		$this->assertEquals("\r\none\r\ntwo\r\nthree\r\n", $s->__toString());
	}
	
	/**
	 * Prepend the input to the result string
	 * @param mixed $in
	 */
	public function testPrepend(){
		
		$s = new StringBuilder('one');
		$this->assertEquals('one', $s->__toString());
		$s->prepend('two');
		$this->assertEquals('twoone', $s->__toString());
		$s->prepend('three');
		$this->assertEquals('threetwoone', $s->__toString());
	}
	
	/**
	 * Prepend the input to the result string with an EOL added
	 * @param mixed $in
	 */
	public function testPrependEOL(){
		
		$s = new StringBuilder('one');
		$this->assertEquals('one', $s->__toString());
		$s->prependEOL('two');
		$this->assertEquals("two\r\none", $s->__toString());
		$s->prependEOL('three');
		$this->assertEquals("three\r\ntwo\r\none", $s->__toString());
	
		
	}
	
	public function testAppendNull(){
		$s = new StringBuilder();
		$s->append(null);
	
		$this->assertEquals("", $s->__toString());
	}
	
	/**
	 * Append the oject dump to the string
	 * @param object $obj
	 */
	public function testAppendObject(){
		
		$s = new StringBuilder('one');
		$this->assertEquals('one', $s->__toString());
		
		$obj = (object) ['body' => 'hi there'];
		
		$s->appendObject($obj);
		$this->assertContains(StringConstant::HR, $s->__toString());
		$this->assertContains('one', $s->__toString());
		$this->assertContains('stdClass Object', $s->__toString());
		$this->assertContains('[body] => hi there', $s->__toString());
	}
	
	/**
	 * Return or output the string 
	 * @param boolean $prnt If TRUE, then print the string
	 * @param boolean $nl2br If TRUE, the output is converted: \n -> &lt;br&gt;
	 * @return string output
	 */
	public function testToStringReturn($prnt=false, $nl2br = false){
		$expected = "one<br />\r\n";
		
		$s = new StringBuilder();
		$s->appendEOL('one');
		
		$this->assertEquals("one\r\n", $s->toString(false,false));
		$this->assertEquals($expected, $s->toString(false,true));
		ob_start();
		$s->toString(true,true);
		$html = ob_get_contents();
		ob_end_clean();
		
		$this->assertEquals($expected, $html);
		
		$this->assertEquals($expected, $s->nl2br());
		
		ob_start();
		$s->nl2br(StringBuilder::PRINT_STR);
		$html = ob_get_contents();
		ob_end_clean();
		
		$this->assertEquals($expected, $html);
	}
	
	public function testIsEmpty(){
		
		$s = new StringBuilder();
		$s->append();
		$this->assertEquals('', $s->__toString());
		$s->append('one');
		$this->assertEquals('one', $s->__toString());
		
		$this->assertFalse($s->isEmpty());
		
		$s = new StringBuilder();
		$s->append();
		$this->assertTrue($s->isEmpty());
		
		$s = new StringBuilder();
		$s->append(null);
		$s->append(null);
		$s->append(null);
		
		$this->assertTrue($s->isEmpty());
		
		$s = new StringBuilder();
		$s->append(null);
		$s->append(null);
		$s->appendEOL(null);
		
		$this->assertFalse($s->isEmpty());
		
		
		
		
	}
	
	public function testClear(){
		$s = new StringBuilder();
		$s->append();
		$this->assertEquals('', $s->__toString());
		$s->append('one');
		$this->assertEquals('one', $s->__toString());
		
		$s->clear();
		$this->assertEquals('', $s->__toString());
		
	}
	
	public function testToJSONString(){
		$s = new StringBuilder();
		$s->append('one');
		$s->append('two');
		$this->assertEquals('onetwo', $s->__toString());
		
		$this->assertNotEquals(false, json_encode($s->raw()));
	
	}
	
	public function testSanitizing(){
		$s = new StringBuilder();
		$s->append(null);
		$s->append(null);
		$s->append(null);
		$this->assertEquals(0, strlen($s->__toString()));
		$this->assertEquals(0, strlen($s->toString()));
		$s->append('');
		$this->assertEquals(0, strlen($s->__toString()));
		$this->assertEquals(0, strlen($s->toString()));
		$s->appendEOL(null);
		$this->assertEquals(2, strlen($s->__toString()));
		$this->assertEquals(2, strlen($s->toString()));
		$s->append('one');
		$this->assertEquals(5, strlen($s->__toString()));
		$this->assertEquals(5, strlen($s->toString()));
		
		$s->appendEOL('two');
		$this->assertEquals(10, strlen($s->__toString()));
		$this->assertEquals(10, strlen($s->toString()));
		
		$this->assertEquals("\r\nonetwo\r\n", $s->toString());
		$this->assertEquals($s->toString(), $s->__toString());
		
		
	}
	
	public function testRaw(){
		$s = new StringBuilder();
		$this->assertTrue(is_array($s->raw()));
		$s->append(null);
		$s->append(null);
		$s->append(null);
		$this->assertEquals(3, count($s->raw()));
		$s->append('');
		$this->assertEquals(4, count($s->raw()));
		$s->appendEOL(null);
		$this->assertEquals(5, count($s->raw()));
		$s->appendEOL('one');
		$this->assertEquals(6, count($s->raw()));
		
	
	
	}
	
}

?>