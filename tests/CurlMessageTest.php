<?php

if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
require_once ROOT.'/tests/TestCase.php';
require_once ROOT.'/tests/resources/TestData.php';


/**
 * @runTestsInSeparateProcesses
 */
class CurlMessageTest extends TestCase {

	
private $error;
private $message;
	
	public function setup(){
		//catching user errors for assertion
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$this->error =  "\n".$errstr . " on line " . $errline . " in file " . $errfile."\n";
		});
		$this->message = $m = TestData::getTestRequest();
	}

	public function teardown(){
		$this->message = null;
		print $this->error;
		restore_error_handler();
	}
	
	/**
	 * Check if testmessage is configured properly
	 */
	public function testMessage(){
		$this->assertEquals(TestData::ID, $this->message->getID());
		$this->assertEquals(TestData::MESSAGE_NAME, $this->message->getName());
	}
	
	/**
	 * Retrieve the internal persistence message ID. This is usually a generated UUID.
	 * @see Utils\Util::createUUID() method
	 * @return string  The UUID
	 */
	public function testGetID(){
		$this->assertNotNull($this->message->getID());
		$this->assertEquals(TestData::ID, $this->message->getID());
	}
	
	/**
	 * Set the internal persistence message ID. This is usually a generated UUID.
	 * @see Utils\Util::createUUID() method
	 * @param string $id The UUID
	 */
	public  function testSetID(){
		$this->message->setID(null);
		$this->assertNull($this->message->getID());
		$this->message->setID(TestData::ID);
		$this->assertEquals(TestData::ID, $this->message->getID());
	}
	
	/**
	 * Get the success status
	 * @return boolean
	 */
	public function testIsSuccess(){
		$this->assertTrue($this->message->isSuccess());
	}
	/**
	 * Set the success status.
	 * @param boolean $success
	 */
	public function testSetSuccess(){
		$this->message->setSuccess(false);
		$this->assertFalse($this->message->isSuccess());
	}
	
	/**
	 * Get the error messages
	 */
	public function testGetError(){
		$this->assertNull($this->message->getError());
	}
	
	/**
	 * Set the error messages
	 * @param String $error
	 */
	public function testSetError(){
		$this->message->setError([12]);
		$this->assertFalse(empty($this->message->getError()));
		$this->assertTrue(is_array($this->message->getError()));
		$this->assertEquals([12], $this->message->getError());
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function testToArray() {
		
		$arr = $this->message->toArray();
		$this->assertEquals(7, count($arr));
		
		$this->assertArrayHasKey('message_id', $arr);
		$this->assertEquals(TestData::ID, $arr['message_id']);
		
		$this->assertArrayHasKey('name', $arr);
		$this->assertEquals(TestData::MESSAGE_NAME, $arr['name']);
		
		$this->assertArrayHasKey('payload', $arr);
		$this->assertEquals(TestData::getPayload(), $arr['payload']);
		
		$this->assertArrayHasKey('notNullable', $arr);
		$this->assertEquals(3, count($arr['notNullable']));
		
		$this->assertArrayHasKey('success', $arr);
		$this->assertTrue($arr['success']);
		
		$this->assertArrayHasKey('error', $arr);
		$this->assertNull($arr['error']);
		
		$this->assertArrayHasKey('report', $arr);
		$this->assertEquals('CurlReport', get_class($arr['report']));
		$this->assertClassHasAttribute('success', get_class($arr['report']));
		$this->assertClassHasAttribute('info', get_class($arr['report']));
		$this->assertClassHasAttribute('warning', get_class($arr['report']));
		$this->assertClassHasAttribute('error', get_class($arr['report']));
	}
	
	/**
	 * Revive the original message from the request object
	 *
	 * Ignored properties are clazz, created_at and updated_at
	 *
	 * @param array $request  The request array
	 * @return CurlMessage $message  The revived message
	 */
	public function testRevive(){
		$message = new TestMessage();
		$this->assertNotEquals($this->message, $message);
		
		$message->revive($this->message->toArray());
		$this->assertEquals($this->message, $message);
		
		$message = new TestMessage();
		$message->revive(null);
		$this->assertContains('NullArgumentException', $message->getReport()->__toString());
		
		$message = new TestMessage();
		$m01 = new TestMessage();
		$m01->revive($this->message->toArray());
		$m01->added = null;
		$message->revive($m01);
		$this->assertContains('PropertyException', $message->getReport()->__toString());
		
		
		$message = new TestMessage();
		$message->revive('fault');
		$this->assertContains('InvalidTypeException', $message->getReport()->__toString());

	}
	
	public function testHasFailed(){
		
		$message = new TestMessage();
		$this->assertNotEquals($this->message, $message);
		
		$message->revive($this->message->toArray());
		$this->assertEquals($this->message, $message);
		$this->assertFalse($message->hasFailed());
		
		$message = new TestMessage();
		$message->revive(null);
		$this->assertContains('NullArgumentException', $message->getReport()->__toString());
		$this->assertTrue($message->hasFailed());
		
		
	}
	
	/**
	 * Validate the current message
	 *
	 * @param array $warnIfNull  The properties as array for which to warn if the content is NULL
	 * @return CurlMessageValidation  The validation report
	 */
	public function testValidate(){
	
	}
	
	public function testExtending(){
		$m = TestData::getTestRequest();

		$this->assertEquals(TestData::ID, $m->getID());
		$this->assertEquals(TestData::MESSAGE_NAME, $m->getName());
		
	}
}

?>