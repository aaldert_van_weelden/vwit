<?php

if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
require_once ROOT.'/tests/TestCase.php';


/**
 * @runTestsInSeparateProcesses
 */
class CurlReportTest extends TestCase {

	
private $error;
	
	public function setup(){
		//catching user errors from Enum for assertion
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$this->error =  "\n".$errstr . " on line " . $errline . " in file " . $errfile."\n";
		});
	}

	public function teardown(){
		restore_error_handler();
	}
	
	public function testToString(){
	
		$report = new CurlReport();
		$this->assertTrue(empty($report->__toString()));
		$report->success->append('success');
		$this->assertEquals(" ||SUCCESS: success", $report->__toString());
		$report->info->append('info');
		$this->assertEquals(" ||SUCCESS: success ||INFO: info", $report->__toString());
		$report->warning->append('warning');
		$this->assertEquals(" ||SUCCESS: success ||INFO: info ||WARNING: warning", $report->__toString());
		$report->error->append('error');
		$this->assertEquals(" ||SUCCESS: success ||INFO: info ||WARNING: warning ||ERROR: error", $report->__toString());
		
		
		
		
	}
	
	public function testRaw(){
	
		$report = new CurlReport();
		$this->assertTrue(is_array($report->raw()));
		$this->assertEquals(['success'=>[], 'info'=>[], 'warning'=>[], 'error'=>[]], $report->raw());
		$report->success->append('success');
		$this->assertEquals(['success'=>['success'], 'info'=>[], 'warning'=>[], 'error'=>[]], $report->raw());
		$report->info->append('info');
		$this->assertEquals(['success'=>['success'], 'info'=>['info'], 'warning'=>[], 'error'=>[]], $report->raw());
		$report->warning->append('warning');
		$this->assertEquals(['success'=>['success'], 'info'=>['info'], 'warning'=>['warning'], 'error'=>[]], $report->raw());
		$report->error->append('error');
		$this->assertEquals(['success'=>['success'], 'info'=>['info'], 'warning'=>['warning'], 'error'=>['error']], $report->raw());
	
	}
}

?>