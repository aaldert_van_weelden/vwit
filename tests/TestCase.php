<?php
if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));

require_once ROOT.'/vendor/autoload.php';
require_once ROOT.'/tests/resources/TestData.php';

/**
 * @author Aaldert van Weelden
 *
 */
class TestCase extends \PHPUnit_Framework_TestCase {
	
	
	/**
	 * Default preparation for each test
	 */
	public function setUp(){
		parent::setUp();
		$this->prepareForTests();
	}
	
	/**
	 * Migrate the database
	 */
	private function prepareForTests(){
		
	}
}
