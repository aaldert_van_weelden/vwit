<?php
use Utils\Util;

if(!defined('ROOT')) define ('ROOT', dirname(dirname(__FILE__)));
require_once ROOT.'/tests/TestCase.php';


/**
 * @runTestsInSeparateProcesses
 */
class UtilTest extends TestCase {

	
private $error;
	
	public function setup(){
		//catching user errors from Enum for assertion
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$this->error =  "\n".$errstr . " on line " . $errline . " in file " . $errfile."\n";
		});
	}

	public function teardown(){
		restore_error_handler();
	}
	
	/**
	 * @group utils
	 */
	public function test_guid(){

		$guid = Util::create_GUID();
		
		$this->assertNotNull($guid);
		
		var_dump($guid);
		
		$UUIDv4 = '/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i';
		$this->assertTrue( filter_var( preg_match($UUIDv4, $guid), FILTER_VALIDATE_BOOLEAN ) );
	}
}

?>