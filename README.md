# VWIT foundation #

A VWIT php JS foundation library 

* add dependencies for your project in the composer.json:

		"require": {
			  	"vwit/foundation": "dev-master"
			},
		"repositories": [
		    {
		       "type": "vcs",
		        "url": "https://aaldert_van_weelden@bitbucket.org/aaldert_van_weelden/vwit.git"
		    }
		],
		"psr-0": {
			  "Foundation": "vendor/vwit/foundation/lib/"    
		},

## What is this library for? ##

### PHP classes ###

* helper classes
* logger classes
* base classes
* datetime utils
* sql utils
* command classes

### Javascript classes ###

Use copy2assets util  to transfer JS classes form vendor library to the application's public assets directory

Usage:  php copy2assets.php [assetpath\relative\to\applicationroot]  eg  public\assets for laravel

### Updating project: ###

* After commiting changes to the foundation, delete the vendor/vwit directory.
* Lookup the VWIT commit hash in the composer.lock file
* Replace the hash wit the commit you want to load into the applicatio/vendor directory
* run composer install
* run php vendor/vwit/foundation/copy2assets.php  public\assets  in order to transer the asset JS classes OR:
* add the following section to your application root composer.json :

		"scripts": {
			"post-install-cmd": [
				"php vendor/vwit/foundation/copy2assets.php public/assets"
			]
		}
		

Private licence is applied